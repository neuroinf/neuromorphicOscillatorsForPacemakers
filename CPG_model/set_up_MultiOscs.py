# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

from neuromorphicOscillatorsForPacemakers.CPG_model.MultipleOscillatorModel import MultipleOscillatorModel

def set_up_MultiOscs(NetParams, c, CtxDynapse):
    ''' method used to set up network of coyupled oscillators on DYNAP-SE board '''
    # NeuronNeuronConnector = c.modules.NeuronNeuronConnector
    MyOscs = MultipleOscillatorModel(N_exc=NetParams.N_exc,
                                     N_inh=NetParams.N_inh,
                                     neuron_ids_global_start_exc=NetParams.neuron_ids_global_start_exc,
                                     neuron_ids_global_start_inh=NetParams.neuron_ids_global_start_inh,
                                     syntypes_exc2exc=NetParams.syntypes_exc2exc,
                                     syntypes_exc2inh=NetParams.syntypes_exc2inh,
                                     syntypes_inh2exc=NetParams.syntypes_inh2exc,
                                     syntypes_excA2excB=NetParams.syntypes_excA2excB,
                                     syntypes_inhA2inhB=NetParams.syntypes_inhA2inhB,
                                     syntypes_excA2inhB=NetParams.syntypes_excA2inhB,
                                     syntypes_inhA2excB=NetParams.syntypes_inhA2excB,
                                     n_conn_exc2exc=NetParams.n_conn_exc2exc,
                                     n_conn_exc2inh=NetParams.n_conn_exc2inh,
                                     n_conn_inh2exc=NetParams.n_conn_inh2exc,
                                     n_conn_excA2excB=NetParams.n_conn_excA2excB,
                                     n_conn_inhA2inhB=NetParams.n_conn_inhA2inhB,
                                     n_conn_excA2inhB=NetParams.n_conn_excA2inhB,
                                     n_conn_inhA2excB=NetParams.n_conn_inhA2excB,
                                     conn_strength_exc2exc=NetParams.conn_strength_exc2exc,
                                     conn_strength_exc2inh=NetParams.conn_strength_exc2inh,
                                     conn_strength_inh2exc=NetParams.conn_strength_inh2exc,
                                     conn_strength_excA2excB=NetParams.conn_strength_excA2excB,
                                     conn_strength_inhA2inhB=NetParams.conn_strength_inhA2inhB,
                                     conn_strength_excA2inhB=NetParams.conn_strength_excA2inhB,
                                     conn_strength_inhA2excB=NetParams.conn_strength_inhA2excB,
                                     random_seed=NetParams.random_seed,
                                     CtxDynapse=CtxDynapse,
                                     NeuronNeuronConnector=c.modules.NeuronNeuronConnector,
                                     SynapseTypes=CtxDynapse.DynapseCamType)

    MyOscs.set_up_dynapse_model()
    MyOscs.set_up_connections(interosc_connection_types=NetParams.interosc_connection_types)

    return MyOscs
