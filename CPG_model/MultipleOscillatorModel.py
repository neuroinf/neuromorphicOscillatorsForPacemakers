# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

try:
    import CtxDynapse
    import NeuronNeuronConnector
    from CtxDynapse import DynapseCamType as SynapseTypes
except:
    CtxDynapse = None
    NeuronNeuronConnector = None
    SynapseTypes = None

import random
import numpy as np


class MultipleOscillatorModel(object):
    def __init__(self, N_exc, N_inh, neuron_ids_global_start_exc, neuron_ids_global_start_inh,
                 syntypes_exc2exc, syntypes_exc2inh, syntypes_inh2exc,
                 syntypes_excA2excB, syntypes_inhA2inhB, syntypes_excA2inhB, syntypes_inhA2excB,
                 n_conn_exc2exc=4, n_conn_exc2inh=4, n_conn_inh2exc=4,
                 n_conn_excA2excB=4, n_conn_inhA2inhB=4, n_conn_excA2inhB=4, n_conn_inhA2excB=4,
                 conn_strength_exc2exc=None, conn_strength_exc2inh=None, conn_strength_inh2exc=None,
                 conn_strength_excA2excB=None, conn_strength_inhA2inhB=None, conn_strength_excA2inhB=None, conn_strength_inhA2excB=None,
                 random_seed=None,
                 CtxDynapse=CtxDynapse, NeuronNeuronConnector=NeuronNeuronConnector, SynapseTypes=SynapseTypes):

        self.CtxDynapse = CtxDynapse
        self.NeuronNeuronConnector = NeuronNeuronConnector
        self.SynapseTypes = SynapseTypes

        self.n_oscs = len(N_exc)
        self.N_exc = N_exc
        self.N_inh = N_inh
        self.neuron_ids_global_start_exc = neuron_ids_global_start_exc
        self.neuron_ids_global_start_inh = neuron_ids_global_start_inh

        self.syntypes_exc2exc = syntypes_exc2exc
        self.syntypes_exc2inh = syntypes_exc2inh
        self.syntypes_inh2exc = syntypes_inh2exc
        self.syntypes_excA2excB = syntypes_excA2excB
        self.syntypes_inhA2inhB = syntypes_inhA2inhB
        self.syntypes_excA2inhB = syntypes_excA2inhB
        self.syntypes_inhA2excB = syntypes_inhA2excB

        self.n_conn_exc2exc = self._extend_to_N_osc_if_None(n_conn_exc2exc)
        self.n_conn_exc2inh = self._extend_to_N_osc_if_None(n_conn_exc2inh)
        self.n_conn_inh2exc = self._extend_to_N_osc_if_None(n_conn_inh2exc)
        self.n_conn_excA2excB = self._extend_to_N_osc_if_None(n_conn_excA2excB)
        self.n_conn_inhA2inhB = self._extend_to_N_osc_if_None(n_conn_inhA2inhB)
        self.n_conn_excA2inhB = self._extend_to_N_osc_if_None(n_conn_excA2inhB)
        self.n_conn_inhA2excB = self._extend_to_N_osc_if_None(n_conn_inhA2excB)

        # num connections to set on DYANPse
        self.conn_strength_exc2exc = self._extend_to_N_osc_if_None(conn_strength_exc2exc)
        self.conn_strength_exc2inh = self._extend_to_N_osc_if_None(conn_strength_exc2inh)
        self.conn_strength_inh2exc = self._extend_to_N_osc_if_None(conn_strength_inh2exc)
        self.conn_strength_excA2excB = self._extend_to_N_osc_if_None(conn_strength_excA2excB)
        self.conn_strength_inhA2inhB = self._extend_to_N_osc_if_None(conn_strength_inhA2inhB)
        self.conn_strength_excA2inhB = self._extend_to_N_osc_if_None(conn_strength_excA2inhB)
        self.conn_strength_inhA2excB = self._extend_to_N_osc_if_None(conn_strength_inhA2excB)

        if random_seed is not None:
            random.seed(random_seed)


    def _extend_to_N_osc_if_None(self, value, default_value=1):
        if value is None:
            return [default_value]*self.n_oscs
        else:
            return value


    def set_up_dynapse_model(self, dynapse_model=None):
        if dynapse_model is None:
            self.dynapse_model = self.CtxDynapse.model
        else:
            self.dynapse_model = dynapse_model


    def set_up_connector(self, connector=None):
        if connector is None:
            self.connector = self.NeuronNeuronConnector.DynapseConnector()
        else:
            self.connector = connector


    def _get_pre_and_post_dynapse_neurons(self, global_pre_ids, global_post_ids, n_conn):
        pre_neurons, post_neurons = [], []
        if n_conn > 0:
            for post_id in global_post_ids:

                if n_conn <= len(global_pre_ids):
                    # to make at most one connection per pre neuron
                    pre_ids = np.random.choice(global_pre_ids, size=n_conn, replace=False).tolist()
                else:
                    # to make at most one connection per pre neuron and only then it
                    # can be picked again
                    pre_ids = global_pre_ids * (n_conn//len(global_pre_ids))
                    pre_ids = pre_ids + np.random.choice(global_pre_ids, size=n_conn%len(global_pre_ids), replace=False).tolist()

                for pre_id in pre_ids:
                    pre_neurons.append(self.dynapse_neurons[pre_id])
                post_neurons = post_neurons + [self.dynapse_neurons[post_id]] * n_conn
        return pre_neurons, post_neurons


    def _set_up_connections_within_oscillator(self):
        # set up connections within individual oscillators
        for osc_nr in range(self.n_oscs):
            # EXC2EXC
            conn_strength = self.conn_strength_exc2exc[osc_nr]
            pre_neurons_exc2exc, post_neurons_exc2exc = self._get_pre_and_post_dynapse_neurons(global_pre_ids=self.global_neuron_ids_exc[osc_nr],
                                                                                               global_post_ids=self.global_neuron_ids_exc[osc_nr],
                                                                                               n_conn=self.n_conn_exc2exc[osc_nr])
            exc2exc_synapse_types = [self.syntypes_exc2exc[osc_nr]] * len(pre_neurons_exc2exc)
            self.connector.add_connection_from_list(pre_neurons_exc2exc*conn_strength,
                                                    post_neurons_exc2exc*conn_strength,
                                                    exc2exc_synapse_types*conn_strength)

            # EXC2INH
            conn_strength = self.conn_strength_exc2inh[osc_nr]
            pre_neurons_exc2inh, post_neurons_exc2inh = self._get_pre_and_post_dynapse_neurons(global_pre_ids=self.global_neuron_ids_exc[osc_nr],
                                                                                               global_post_ids=self.global_neuron_ids_inh[osc_nr],
                                                                                               n_conn=self.n_conn_exc2inh[osc_nr])
            exc2inh_synapse_types = [self.syntypes_exc2inh[osc_nr]] * len(pre_neurons_exc2inh)
            self.connector.add_connection_from_list(pre_neurons_exc2inh*conn_strength,
                                                    post_neurons_exc2inh*conn_strength,
                                                    exc2inh_synapse_types*conn_strength)

            # INH2EXC
            conn_strength = self.conn_strength_inh2exc[osc_nr]
            pre_neurons_inh2exc, post_neurons_inh2exc = self._get_pre_and_post_dynapse_neurons(global_pre_ids=self.global_neuron_ids_inh[osc_nr],
                                                                                               global_post_ids=self.global_neuron_ids_exc[osc_nr],
                                                                                               n_conn=self.n_conn_inh2exc[osc_nr])
            inh2exc_synapse_types = [self.syntypes_inh2exc[osc_nr]] * len(pre_neurons_inh2exc)
            self.connector.add_connection_from_list(pre_neurons_inh2exc*conn_strength,
                                                    post_neurons_inh2exc*conn_strength,
                                                    inh2exc_synapse_types*conn_strength)


    def _set_up_connections_between_oscillators(self, connection_types):
        if 'excA2excB' in connection_types:
            print('Add excA2excB connections')
            # set up exc connections between individual oscillators
            for osc_nr_pre in range(self.n_oscs):
                osc_nr_post = ((osc_nr_pre+1)%self.n_oscs)
                conn_strength = self.conn_strength_excA2excB[osc_nr_post]
                pre_neurons, post_neurons = self._get_pre_and_post_dynapse_neurons(global_pre_ids=self.global_neuron_ids_exc[osc_nr_pre],
                                                                                   global_post_ids=self.global_neuron_ids_exc[osc_nr_post],
                                                                                   n_conn=self.n_conn_excA2excB[osc_nr_post])
                synapse_types = [self.syntypes_excA2excB[osc_nr_post]] * len(pre_neurons)
                self.connector.add_connection_from_list(pre_neurons*conn_strength, post_neurons*conn_strength, synapse_types*conn_strength)

        if 'inhA2inhB' in connection_types:
            print('Add inhA2inhB connections')
            # set up inh connections between individual oscillators(only to preceeding partner)
            for osc_nr_pre in range(self.n_oscs):
                osc_nr_post = ((osc_nr_pre+1)%self.n_oscs)
                conn_strength = self.conn_strength_excA2excB[osc_nr_post]
                pre_neurons, post_neurons = self._get_pre_and_post_dynapse_neurons(
                        global_pre_ids=self.global_neuron_ids_inh[osc_nr_pre],
                        global_post_ids=self.global_neuron_ids_inh[osc_nr_post],
                        n_conn=self.n_conn_inhA2inhB[osc_nr_post])
                synapse_types = [self.syntypes_inhA2inhB[osc_nr_post]] * len(pre_neurons)
                self.connector.add_connection_from_list(pre_neurons*conn_strength,
                                                        post_neurons*conn_strength,
                                                        synapse_types*conn_strength)

        if 'excA2inhB' in connection_types:
            print('Add excA2inhB connections')
            # set up exc connections to inh population of other oscillator
            for osc_nr_pre in range(self.n_oscs):
                for osc_nr_post in range(self.n_oscs):
                    if osc_nr_pre != osc_nr_post: # do not add another connection to own inh population
                        conn_strength = self.conn_strength_excA2excB[osc_nr_post]
                        pre_neurons, post_neurons = self._get_pre_and_post_dynapse_neurons(
                            global_pre_ids=self.global_neuron_ids_exc[osc_nr_pre],
                            global_post_ids=self.global_neuron_ids_inh[osc_nr_post],
                            n_conn=self.n_conn_excA2inhB[osc_nr_post])
                        synapse_types = [self.syntypes_excA2inhB[osc_nr_post]] * len(pre_neurons)
                        self.connector.add_connection_from_list(pre_neurons*conn_strength,
                                                                post_neurons*conn_strength,
                                                                synapse_types*conn_strength)

        if 'inhA2excB' in connection_types:
            print('Add inhA2excB connections')
            # set up inh connections to exc population of other oscillator
            for osc_nr_pre in range(self.n_oscs):
                for osc_nr_post in range(self.n_oscs):
                    if osc_nr_pre != osc_nr_post: # do not add another connection to own inh population
                        conn_strength = self.conn_strength_excA2excB[osc_nr_post]
                        pre_neurons, post_neurons = self._get_pre_and_post_dynapse_neurons(
                            global_pre_ids=self.global_neuron_ids_inh[osc_nr_pre],
                            global_post_ids=self.global_neuron_ids_exc[osc_nr_post],
                            n_conn=self.n_conn_inhA2excB[osc_nr_post]
                        )
                        synapse_types = [self.syntypes_inhA2excB[osc_nr_post]] * len(pre_neurons)
                        self.connector.add_connection_from_list(pre_neurons*conn_strength,
                                                                post_neurons*conn_strength,
                                                                synapse_types*conn_strength)



    def set_up_connections(self, interosc_connection_types):
        ''' create connections such that every post-synaptic neuron is conneted to n_conn pre-synaptic neurons (guaranteed to
        be connected to n_conn (in general self.n_connections) neurons in total. '''

        self.set_up_connector()

        self.dynapse_neurons = self.dynapse_model.get_shadow_state_neurons()

        self.global_neuron_ids_exc = []
        self.global_neuron_ids_inh = []
        for osc_nr in range(self.n_oscs):
            self.global_neuron_ids_exc.append(range(self.neuron_ids_global_start_exc[osc_nr],
                                           self.neuron_ids_global_start_exc[osc_nr] + self.N_exc[osc_nr]))
            self.global_neuron_ids_inh.append(range(self.neuron_ids_global_start_inh[osc_nr],
                                           self.neuron_ids_global_start_inh[osc_nr] + self.N_inh[osc_nr]))

        self._set_up_connections_within_oscillator()
        self._set_up_connections_between_oscillators(connection_types=interosc_connection_types)

        self.dynapse_model.apply_diff_state()
        print('Connections setup and applied')


    def set_up_spikegenerator(self):
        ''' Instantiate spikegenerator '''
        self.virtual_model = self.CtxDynapse.VirtualModel()
        self.virtual_neurons = self.virtual_model.get_neurons()
        self.spikegen = self.CtxDynapse.model.get_fpga_modules()[1]


    def add_connections_to_spikegenerator(self, virtual_neuron_ids, target_neuron_ids_global_per_osc,
                                                   syn_type, conn_strength):
        ''' Create connections for spike generator from virtual neurons to target_neuron_ids
                fully connected with desired connections strength (conn_strength) and synapse_type (syn_type) '''

        for osc_nr, _ in enumerate(target_neuron_ids_global_per_osc):
            pre_neurons, post_neurons = [], []
            for post_neuron_nr, post_neuron_id_global in enumerate(target_neuron_ids_global_per_osc[osc_nr]):
                post_neurons.extend([self.dynapse_neurons[post_neuron_id_global]]*len(virtual_neuron_ids))
                for virtual_neuron_id in virtual_neuron_ids:
                    pre_neurons.append(self.virtual_neurons[virtual_neuron_id])
            synapse_types = [syn_type] * len(pre_neurons)
            self.connector.add_connection_from_list(pre_neurons*conn_strength, post_neurons*conn_strength,
                                                      synapse_types*conn_strength)
        self.dynapse_model.apply_diff_state()
