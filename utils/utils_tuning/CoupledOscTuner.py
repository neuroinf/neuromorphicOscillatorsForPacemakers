# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np

from neuromorphicOscillatorsForPacemakers.CPG_model.ActivationTimeExtractor import ActivationTimeExtractor
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.utils_hardwareTuning import increase_bias_value, decrease_bias_value
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import get_events_within_dt
from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter

ATE  = ActivationTimeExtractor()
OPlt = OscillatorPlotter()

class CoupledOscTuner(object):
    """ class used to tune frequency and phase shift of coupled oscillators """

    def __init__(self,
                 dt_init,
                 dt_run,

                 step_size_strong,
                 step_size_weak,
                 delay_diff_considered_large,

                 tolerated_diff_for_osc_ok,
                 tolerated_std_for_osc_ok,
                 step_size_if_osc_not_ok,

                 thr_exc, thr_inh, dt, tau,

                 N_osc,
                 neuron_ids_global_pops_exc,
                 neuron_ids_global_pops_inh,
                 core_ids_global_pop_exc,
                 core_ids_global_pop_inh,
                 current_EventBuffer,
                 CtxDynapse,

                 snapshot_every):

        self.dt_init = dt_init
        self.dt_run  = dt_run

        self.step_size_strong = step_size_strong
        self.step_size_weak   = step_size_weak
        self.delay_diff_considered_large = delay_diff_considered_large

        self.tolerated_diff_for_osc_ok = tolerated_diff_for_osc_ok
        self.tolerated_std_for_osc_ok  = tolerated_std_for_osc_ok
        self.step_size_if_osc_not_ok   = step_size_if_osc_not_ok

        self.thr_exc = thr_exc
        self.thr_inh = thr_inh
        self.dt      = dt
        self.tau     = tau

        self.N_osc = N_osc
        self.neuron_ids_global_pops_exc = neuron_ids_global_pops_exc
        self.neuron_ids_global_pops_inh = neuron_ids_global_pops_inh
        self.core_ids_global_pop_exc = core_ids_global_pop_exc
        self.core_ids_global_pop_inh = core_ids_global_pop_inh
        self.current_EventBuffer = current_EventBuffer
        self.CtxDynapse = CtxDynapse

        self.snapshot_every = snapshot_every


    def _get_t_thr_crossings(self):
        all_events = get_events_within_dt(dt_init=self.dt_init, dt_run=self.dt_run,
                                          current_EventBuffer=self.current_EventBuffer,
                                          CtxDynapse=self.CtxDynapse,
                                          verbose=False)

        MyEMs           = OPlt.events_to_EMs_of_coupled_oscs(all_events=all_events,
                                                             N_osc=self.N_osc,
                                                             neuron_ids_global_pops_exc=self.neuron_ids_global_pops_exc,
                                                             neuron_ids_global_pops_inh=self.neuron_ids_global_pops_inh)

        MySVMs          = OPlt.EMs_to_SVMs_Atraces_of_coupled_osc(MyEMs,
                                                                  N_osc=self.N_osc,
                                                                  dt=self.dt,
                                                                  tau=self.tau)

        t_thr_crossings = OPlt.SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(MySVMs=MySVMs,
                                                                                     N_osc=self.N_osc,
                                                                                     thr_exc=self.thr_exc,
                                                                                     thr_inh=self.thr_inh,
                                                                                     verbose=False)

        return t_thr_crossings


    def coupled_oscillators_are_balanced(self, t_thr_crossings, full_cycle_delay, verbose):
        """ return boolean saying whether the coupled oscillators are all running with a similar frequency
        and that none of the individual oscillator shows a unstable behaviour (=high std in frequency)"""
        if verbose:
            for osc_nr in range(self.N_osc):
                name_exc = 'exc' + str(int(osc_nr))
                print('isi exc {}: {} +- {}'.format(osc_nr,
                                                    np.mean(np.diff(t_thr_crossings[name_exc].flatten())),
                                                    np.std(np.diff(t_thr_crossings[name_exc].flatten()))))

        oscillators_balanced = True
        for osc_nr in range(self.N_osc):
            name_exc = 'exc' + str(int(osc_nr))
            ISI_t_thr = np.diff(t_thr_crossings[name_exc].flatten()).flatten()
            if np.abs((np.mean(ISI_t_thr) - full_cycle_delay)) > self.tolerated_diff_for_osc_ok or np.std(ISI_t_thr) > self.tolerated_std_for_osc_ok:
                oscillators_balanced = False
        return oscillators_balanced


    def _reduce_W_to_retrieve_stable_osc(self, dynapse_model, W_excA2excB_fine_values, W_excA2excB_coarse_values, verbose):

        print('oscillations not ok')
        for osc_nr in range(self.N_osc):
            bias_group_nr = self.core_ids_global_pop_exc[osc_nr]
            for i in range(self.step_size_if_osc_not_ok):
                W_excA2excB_fine_values[osc_nr], W_excA2excB_coarse_values[osc_nr] = decrease_bias_value(fine_value=W_excA2excB_fine_values[osc_nr],
                                                                                                         coarse_value=W_excA2excB_coarse_values[osc_nr], verbose=verbose)
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_EXC_F_N', W_excA2excB_fine_values[osc_nr], W_excA2excB_coarse_values[osc_nr])


        return W_excA2excB_fine_values, W_excA2excB_coarse_values


    def _get_difference_to_target_delays(self, t_thr_crossings, target_delays, full_cycle_delay, verbose):

        diffs_to_target_delays = {}
        for delay_name, target_delay in target_delays.items():
            name_oscA, name_oscB = delay_name.split('-')
            t_thr_oscA = t_thr_crossings[name_oscA].flatten()
            t_thr_oscB = t_thr_crossings[name_oscB].flatten()
            dt_popA_to_popB, dt_popB_to_popA = ATE.get_delays_between_two_populations(activation_times_popA=np.asarray(t_thr_oscA, dtype=int),
                                                                                      activation_times_popB=np.asarray(t_thr_oscB, dtype=int),
                                                                                      check_alternation=False)
            diffs_to_target_delays[name_oscA+'-'+name_oscB] = np.mean(dt_popA_to_popB) - target_delay
            if self.N_osc>2:
                diffs_to_target_delays[name_oscB+'-'+name_oscA] = np.mean(dt_popB_to_popA) - (full_cycle_delay-target_delay)

            if verbose:
                print('delay for', delay_name, ':', diffs_to_target_delays[delay_name]+target_delay,
                      'instead of target =', target_delay)

        return diffs_to_target_delays


    def _all_delays_ok(self, diffs_to_target_delays, target_delays, toleranted_diff):

        all_delays_ok = True
        for delay_name, target_delay in target_delays.items():
            if np.abs(diffs_to_target_delays[delay_name]) > toleranted_diff:
                all_delays_ok = False

        return all_delays_ok


    def _reformat_to_collect_delay_per_osc(self, diffs_to_target_delays, target_delays):

        diffs_to_target_delays_per_osc = {}
        for delay_name in target_delays.keys():
            name_oscA, name_oscB = delay_name.split('-')
            diffs_to_target_delays_per_osc[name_oscA] = []

        for delay_name, diff_to_target in diffs_to_target_delays.items():
            name_oscA, name_oscB = delay_name.split('-')
            diffs_to_target_delays_per_osc[name_oscB].append(diff_to_target)

        return diffs_to_target_delays_per_osc


    def _define_actions_to_take_per_osc(self, diffs_to_target_delays_per_osc, toleranted_diff, verbose):
        # define actions to take per oscillator and check for contradicting delays
        actions_to_take_per_osc = {}
        oscs_with_contradicting_delays = []
        for osc_name, diffs_to_targets in diffs_to_target_delays_per_osc.items():

            if self.N_osc>2:
                if np.sign(diffs_to_targets[0]) != np.sign(diffs_to_targets[1]):
                    oscs_with_contradicting_delays.append(osc_name)

            actions_to_take_per_osc[osc_name] = []
            for diff_to_target in diffs_to_targets:
                if np.abs(diff_to_target)<= toleranted_diff:
                    actions_to_take_per_osc[osc_name].append('done')

                elif diff_to_target > 0: # too slow / large --> increase
                    if np.abs(diff_to_target)>self.delay_diff_considered_large and osc_name not in oscs_with_contradicting_delays:
                        actions_to_take_per_osc[osc_name].append('strong_increase')
                    else:
                        actions_to_take_per_osc[osc_name].append('weak_increase')

                elif diff_to_target < 0:  #  too fast / short --> decrease
                    if np.abs(diff_to_target)>self.delay_diff_considered_large and osc_name not in oscs_with_contradicting_delays:
                        actions_to_take_per_osc[osc_name].append('strong_decrease')
                    else:
                        actions_to_take_per_osc[osc_name].append('weak_decrease')

        if verbose:
            print('actions to take: ', actions_to_take_per_osc)

        return actions_to_take_per_osc, oscs_with_contradicting_delays


    def _define_final_actions_to_take(self, actions_to_take_per_osc, oscs_with_contradicting_delays, verbose):
        # get all potential actions in one list
        potential_actions_to_take = []
        for _, actions in actions_to_take_per_osc.items():
            potential_actions_to_take.extend(actions)

        final_actions_to_take = {}
        # check if strong action can be taken
        # ok if not contradictive and if can be counteracted with increase/decrease pair
        if 'strong_increase' in potential_actions_to_take and 'strong_decrease' in potential_actions_to_take:
            step_size = self.step_size_strong
            osc_names_strong_increase, osc_names_strong_decrease = [], []
            for osc_name, actions_to_take in actions_to_take_per_osc.items():
                if 'strong_increase' in actions_to_take:
                    osc_names_strong_increase.append(osc_name)
                elif 'strong_decrease' in actions_to_take:
                    osc_names_strong_decrease.append(osc_name)

            if len(osc_names_strong_increase) == 1:
                final_actions_to_take[osc_names_strong_increase[0]] = 'strong_increase'
            else:
                osc_for_strong_increase = max(set(osc_names_strong_increase), key = osc_names_strong_increase.count)
                final_actions_to_take[osc_for_strong_increase] = 'strong_increase'

            if len(osc_names_strong_decrease) == 1:
                final_actions_to_take[osc_names_strong_decrease[0]] = 'strong_decrease'
            else:
                osc_for_strong_decrease = max(set(osc_names_strong_decrease), key = osc_names_strong_decrease.count)
                final_actions_to_take[osc_for_strong_decrease] = 'strong_decrease'
        else:
            step_size = self.step_size_weak
            for osc_name, actions_to_take in actions_to_take_per_osc.items():
                if len(oscs_with_contradicting_delays) == self.N_osc:
                    if 'weak_increase' in actions_to_take or 'strong_increase' in actions_to_take:
                        final_actions_to_take[osc_name] = 'weak_increase'
                    elif 'weak_decrease' in actions_to_take or 'strong_decrease' in actions_to_take:
                        final_actions_to_take[osc_name] = 'weak_decrease'
                elif osc_name not in oscs_with_contradicting_delays:
                    if 'weak_increase' in actions_to_take or 'strong_increase' in actions_to_take:
                        final_actions_to_take[osc_name] = 'weak_increase'
                    elif 'weak_decrease' in actions_to_take or 'strong_decrease' in actions_to_take:
                        final_actions_to_take[osc_name] = 'weak_decrease'

        if verbose:
            print('final actions:', final_actions_to_take)

        return final_actions_to_take, step_size


    def _adjust_biases_based_on_final_actions(self, final_actions_to_take, step_size,
                                              W_excA2excB_fine_values, W_excA2excB_coarse_values,
                                              dynapse_model, verbose):

        for osc_name, final_action in final_actions_to_take.items():
            osc_nr = int(osc_name[-1])
            bias_group_nr = self.core_ids_global_pop_exc[osc_nr]

            if 'increase' in final_action:
                for i in range(step_size):
                    W_excA2excB_fine_values[osc_nr], W_excA2excB_coarse_values[osc_nr] = increase_bias_value(fine_value=W_excA2excB_fine_values[osc_nr],
                                                                                                             coarse_value=W_excA2excB_coarse_values[osc_nr],
                                                                                                             verbose=verbose)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_EXC_F_N',
                                                                        W_excA2excB_fine_values[osc_nr],
                                                                        W_excA2excB_coarse_values[osc_nr])

            if 'decrease' in final_action:
                for i in range(step_size):
                    W_excA2excB_fine_values[osc_nr], W_excA2excB_coarse_values[osc_nr] = decrease_bias_value(fine_value=W_excA2excB_fine_values[osc_nr],
                                                                                                             coarse_value=W_excA2excB_coarse_values[osc_nr],
                                                                                                             verbose=verbose)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_EXC_F_N',
                                                                        W_excA2excB_fine_values[osc_nr],
                                                                        W_excA2excB_coarse_values[osc_nr])

        return W_excA2excB_fine_values, W_excA2excB_coarse_values


    def tune_coupled_osc_delays(self, dynapse_model, target_delays, toleranted_diff,
                        W_excA2excB_fine_values, W_excA2excB_coarse_values, verbose=True):
        """ dynapse_model: MyOscs.dynapse_model """

        full_cycle_delay = np.sum(list(target_delays.values()))

        counter = 0
        while True:
            counter += 1
            t_thr_crossings = self._get_t_thr_crossings()

            # check if oscillation still ok (all three osc have good average ISI)
            oscillation_ok = self.coupled_oscillators_are_balanced(t_thr_crossings=t_thr_crossings,
                                                                   full_cycle_delay=full_cycle_delay,
                                                                   verbose=verbose)
            if not oscillation_ok:  # if oscillation not ok anymore do overall weight reduction
                W_excA2excB_fine_values, W_excA2excB_coarse_values = self._reduce_W_to_retrieve_stable_osc(dynapse_model=dynapse_model,
                                                                                                           W_excA2excB_fine_values=W_excA2excB_fine_values,
                                                                                                           W_excA2excB_coarse_values=W_excA2excB_coarse_values,
                                                                                                           verbose=verbose)

            else: # if overall oscillation is still ok, then do "fine-tuning"
                diffs_to_target_delays = self._get_difference_to_target_delays(t_thr_crossings=t_thr_crossings,
                                                                               target_delays=target_delays,
                                                                               full_cycle_delay=full_cycle_delay,
                                                                               verbose=verbose)

                if self._all_delays_ok(diffs_to_target_delays, target_delays, toleranted_diff):
                    print('DONE', W_excA2excB_fine_values, W_excA2excB_coarse_values)
                    break

                else:  # adjusting Weight
                    diffs_to_target_delays_per_osc = self._reformat_to_collect_delay_per_osc(diffs_to_target_delays=diffs_to_target_delays,
                                                                                             target_delays=target_delays)
                    actions_to_take_per_osc, oscs_with_contradicting_delays = self._define_actions_to_take_per_osc(diffs_to_target_delays_per_osc,
                                                                                                                   toleranted_diff,
                                                                                                                   verbose)

                    final_actions_to_take, step_size = self._define_final_actions_to_take(actions_to_take_per_osc,
                                                                                          oscs_with_contradicting_delays,
                                                                                          verbose)

                    W_excA2excB_fine_values, W_excA2excB_coarse_values \
                        = self._adjust_biases_based_on_final_actions(final_actions_to_take=final_actions_to_take,
                                                                     step_size=step_size,
                                                                     W_excA2excB_fine_values=W_excA2excB_fine_values,
                                                                     W_excA2excB_coarse_values=W_excA2excB_coarse_values,
                                                                     dynapse_model=dynapse_model,
                                                                     verbose=verbose)

                    if counter%self.snapshot_every == 0:
                        for osc_nr in range(self.N_osc):
                            print('\t \t SNAPSHOT (it {}) isi exc {}: {} +- {} for W: ({}, {})'.format(counter, osc_nr,
                               np.mean(np.diff(t_thr_crossings['exc'+str(int(osc_nr))])), np.std(np.diff(t_thr_crossings['exc'+str(int(osc_nr))])),
                               W_excA2excB_fine_values[osc_nr], W_excA2excB_coarse_values[osc_nr]))

