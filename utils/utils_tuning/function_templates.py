# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np

""" collection of standard functions used to fit """

def double_exp_decay(x, a, b, c, d):
    return a * np.exp(-b * x) + c * np.exp(-d*x)

def neg_double_exp_decay(x, a, b, c, d):
    return -a * np.exp(-b * x) - c * np.exp(-d*x)

def logistic_curve(X, b, c, d, e):
    """
    :param b: slope around the inflection point (can be positive or negative and, consequently, Y may increase or decrease as X increases)
    :param c: lower asymptote
    :param d: higher asymptote
    :param e: X value producing a response half-way between d and c
    """
    return c + ((d-c) / (1 + np.exp(-b*(X-e))))

def exp_fct(X, a, k):
    return a * np.exp(k * X)






