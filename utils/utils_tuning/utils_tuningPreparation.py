# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

try:
    import CtxDynapse
except:
    CtxDynapse = None

""" collectin of methods to simplify parameter tuning very specific for DYNAP-SE"""

def get_global_neuron_id(neuron_id_core, core_id, chip_id):
    neurons_per_core = 256
    cores_per_chip   = 4
    global_neuron_id = neuron_id_core + (core_id * neurons_per_core) \
                       + (chip_id * (neurons_per_core*cores_per_chip))
    return global_neuron_id

def clean_dynapse(CtxDynapse=CtxDynapse, chips_to_clean=[]):
    MyDyn = CtxDynapse.dynapse
    for chip_id_clear_cam in chips_to_clean:
        MyDyn.clear_cam(chip_id_clear_cam)
        MyDyn.clear_sram(chip_id_clear_cam)
    return

def set_some_default_and_do_not_touch_biases(dynapse_model, bias_group_nrs):
    for bias_group_nr in bias_group_nrs:
        dynapse_model.get_bias_groups()[bias_group_nr].set_bias('IF_BUF_P', 80, 4)
        dynapse_model.get_bias_groups()[bias_group_nr].set_bias('IF_RFR_N', 200, 4)
        dynapse_model.get_bias_groups()[bias_group_nr].set_bias('IF_TAU2_N', 255, 7)
        dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PULSE_PWLK_P', 106, 4)
        dynapse_model.get_bias_groups()[bias_group_nr].set_bias('R2R_P', 85, 3)

    return

def set_default_tau_thr_biases(dynapse_model, bias_group_nrs, types=['neuron', 'fast_exc', 'slow_exc', 'fast_inh', 'slow_inh']):
    ''' I_thr = 2 * I_tau '''

    for bias_group_nr in bias_group_nrs:
        if 'neuron' in types:
            # neuron
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('IF_TAU1_N', 60, 4)
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('IF_THR_N', 120, 4)

        # synapses
        if 'fast_exc' in types:
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_TAU_F_P', 60, 4)
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_THR_F_P', 120, 4)
        if 'slow_exc' in types:
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_TAU_S_P', 60, 4)
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_THR_S_P', 120, 4)
        if 'fast_inh' in types:
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_TAU_F_P', 60, 4)
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_THR_F_P', 120, 4)
        if 'slow_inh' in types:
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_TAU_S_P', 60, 4)
            dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_THR_S_P', 120, 4)

    return

def set_not_used_neurons_to_tau2(chip_id, active_neuron_ids_chiplevel, CtxDynapse=CtxDynapse):
    '''
    Set all neurons which are not in active_neuron_ids to listen to tau2 so that
    they can be switched off. This function does first set all neurons to tau1 for
    a given chip and then sets the neurons with ids in active_neuron_ids in the
    specified chip (chip_id) to tau2.git
    :param chip_id:
    :param active_neuron_ids_chiplevel:
    :param CtxDynapse:
    :return:
    '''

    # SET TAU2 FOR NOT USED NEURONS
    for core_id in range(4):  #  ensure that at first all neurons in chip_id listen to tau1
        CtxDynapse.dynapse.reset_tau_1(chip_id, core_id)

    # only 1 chip (chip_id) considered
    neuron_ids_chip_with_tau2 = list(range(0, 4*256))
    [neuron_ids_chip_with_tau2.remove(active_neuron_id) for active_neuron_id in active_neuron_ids_chiplevel]
    for tau2_neuron_id in neuron_ids_chip_with_tau2:
        CtxDynapse.dynapse.set_tau_2(chip_id, tau2_neuron_id)
    return
