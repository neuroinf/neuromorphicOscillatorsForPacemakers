# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.BiasValueConverter import BiasValueConverter
BVC = BiasValueConverter()

def increase_bias_value(fine_value, coarse_value, verbose=True):
    """ get bias value and increase by minimal amount possible """
    max_fine, max_coarse = 255, 7

    if fine_value == 255:
        step_size = BVC.coarse_scales[coarse_value] / 255.
        current = BVC.get_current(fine_value=fine_value, coarse_value=coarse_value)
        new_coarse_value = coarse_value
        while not new_coarse_value == (coarse_value+1):
            current += step_size
            new_fine_value, new_coarse_value = BVC.get_bias_values(current, verbose=verbose)
            if (new_fine_value, new_coarse_value) == (max_fine, max_coarse):
                break
    else:
        new_fine_value = fine_value + 1
        new_coarse_value = coarse_value
    return new_fine_value, new_coarse_value


def decrease_bias_value(fine_value, coarse_value, verbose=True):
    """ get bias value and decrease by minimal amount possible """
    min_fine, min_coarse = 0, 0

    if fine_value == 0:
        step_size = (BVC.coarse_scales[0] / 255.)  # set to stepsize of smallest coarse value
        current = BVC.get_current(fine_value=fine_value+10, coarse_value=coarse_value)  #  increase fine value by random number to get away from 0
        new_coarse_value = coarse_value
        while not new_coarse_value == (coarse_value-1):
            current -= step_size
            new_fine_value, new_coarse_value = BVC.get_bias_values(current, verbose=verbose)
            if (new_fine_value, new_coarse_value) == (min_fine, min_coarse):
                break
    else:
        new_fine_value = fine_value - 1
        new_coarse_value = coarse_value
    return new_fine_value, new_coarse_value

def set_biases(self, dynapse_model, bias_group_nr, bias_names_to_values, verbose=False):

    for bias_name, bias_values in bias_names_to_values.items():
        dynapse_model.get_bias_groups()[bias_group_nr].set_bias(bias_name, bias_values[0], bias_values[1])

    if verbose:
        print('Biases set')


