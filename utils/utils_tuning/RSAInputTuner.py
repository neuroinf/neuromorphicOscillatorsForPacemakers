# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import matplotlib.pylab as plt
import heapq

from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.DynapseOffSwitcher import DynapseOffSwitcher
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrSpikegen import get_events_incl_spikegen_onset_delay_readout, \
                                                                                correct_spiketimes_for_spike_generator_onset_delay
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.utils_hardwareTuning import set_biases
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.utils_RSARestoration import run_RSACPG_over_one_rec
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.function_templates import double_exp_decay

OPlt = OscillatorPlotter()

def round_to_base(x, base=5):
    return base * round(x/base)

CORES_PER_CHIP = 4


class RSAInputTuner(object):
    """ Class to tune system of cooupled oscillators on DYNAP-SE to receive
            inhibitory input and reproduce overall effect of RSA """

    def __init__(self,
                 N_osc,
                 dynapse_model, spikegen,
                 N_exc, N_inh, core_ids_global_pop_exc, core_ids_global_pop_inh,
                 path_to_biases, params_oscs_freqs,
                 ECG_trace,
                 max_rec_duration, n_recs_required,
                 isi_init,

                 thr_exc, thr_inh, dt, tau,
                 seed_nr_poisson,
                 readout_chip_id, readout_core_id, n_spikes_to_readout,
                 dyn_params_SpikeGenReadout, BuffEF_spikegen_readout,
                 MyRSASG,
                 MyFPGAEventGen,
                 MyBiasSaver,
                 CtxDynapse):

        self.N_osc = N_osc
        self.dynapse_model = dynapse_model
        self.spikegen      = spikegen

        self.N_exc = N_exc
        self.N_inh = N_inh
        self.core_ids_global_pop_exc = core_ids_global_pop_exc
        self.core_ids_global_pop_inh = core_ids_global_pop_inh
        self.path_to_biases = path_to_biases
        self.params_oscs_freqs = params_oscs_freqs

        self.ECG_trace = ECG_trace
        self.dt_ecg = 1./self.ECG_trace.sampling_rate

        self.max_rec_duration = max_rec_duration
        self.n_recs_required = n_recs_required

        self.isi_init = float(isi_init)

        self.thr_exc = thr_exc
        self.thr_inh = thr_inh
        self.dt      = dt
        self.tau     = tau

        self.seed_nr_poisson = seed_nr_poisson
        self.readout_chip_id = readout_chip_id
        self.readout_core_id = readout_core_id
        self.n_spikes_to_readout = n_spikes_to_readout
        self.dyn_params_SpikeGenReadout = dyn_params_SpikeGenReadout
        self.BuffEF_spikegen_readout = BuffEF_spikegen_readout

        self.MyRSASG        = MyRSASG
        self.MyFPGAEventGen = MyFPGAEventGen
        self.MyBiasSaver    = MyBiasSaver
        self.CtxDynapse     = CtxDynapse
        self.DynOff         = DynapseOffSwitcher(CtxDynapse=self.CtxDynapse)


    def set_osc_to_given_osc_period(self, osc_period, reload_biases=True):
        if reload_biases:
            bias_set = self.MyBiasSaver.load_biases(dynapse_model=self.dynapse_model,
                                                    inputfilename=self.path_to_biases,
                                                    verbose=False)

        for osc_nr in range(self.N_osc):
            I_DC = double_exp_decay(osc_period, self.params_oscs_freqs[osc_nr][0],
                                    self.params_oscs_freqs[osc_nr][1],
                                    self.params_oscs_freqs[osc_nr][2],
                                    self.params_oscs_freqs[osc_nr][3])*1e3
            self.dynapse_model.get_bias_groups()[self.core_ids_global_pop_exc[osc_nr]].set_linear_bias('IF_DC_P', I_DC)
        return


    def _switch_other_oscillators_off(self, core_id_global_pop_exc,
                                     core_id_global_pop_inh):

        # switch off other oscillators
        core_ids_to_switch_off = list(range(16))
        core_ids_to_switch_off.remove(core_id_global_pop_exc)
        core_ids_to_switch_off.remove(core_id_global_pop_inh)
        self.DynOff.switch_off_all_neurons(dynapse_model=self.dynapse_model,
                                           cores=core_ids_to_switch_off,
                                           verbose=False)

        # switch off coupling input
        for osc_nr in range(self.N_osc):
            self.DynOff.switch_off_specific_synpases(dynapse_model=self.dynapse_model,
                                                     bias_group_nrs=[self.core_ids_global_pop_exc[osc_nr]],
                                                     syn_types=['fast_exc', 'fast_inh'], verbose=False)
            self.DynOff.switch_off_specific_synpases(dynapse_model=self.dynapse_model,
                                                     bias_group_nrs=[self.core_ids_global_pop_inh[osc_nr]],
                                                     syn_types=['slow_exc', 'fast_inh', 'slow_inh'], verbose=False)
        return


    def _get_t_thrs(self, N_osc_used, all_events, all_events_readout,
                               neuron_ids_global_pops_exc, neuron_ids_global_pops_inh):

        MyEMs = OPlt.events_to_EMs_of_coupled_oscs(all_events=all_events, N_osc=N_osc_used,
                                                   neuron_ids_global_pops_exc=neuron_ids_global_pops_exc,
                                                   neuron_ids_global_pops_inh=neuron_ids_global_pops_inh)
        correct_spiketimes_for_spike_generator_onset_delay(all_events_readout=all_events_readout,
                                                           MyEMs_recordings=MyEMs)
        MySVMs = OPlt.EMs_to_SVMs_Atraces_of_coupled_osc(MyEMs, N_osc=N_osc_used, dt=self.dt, tau=self.tau)
        t_thr_crossings = OPlt.SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(MySVMs=MySVMs,
                                                                                     N_osc=N_osc_used,
                                                                                     thr_exc=self.thr_exc,
                                                                                     thr_inh=self.thr_inh,
                                                                                     verbose=False)
        return t_thr_crossings


    def _get_t_thr_delays_over_all_recordings(self, N_osc_used, all_events_over_recs, all_events_readout_over_recs,
                                             all_dt_runs, b_coeffs_trace_per_rec,
                                             neuron_ids_global_pop_exc, neuron_ids_global_pop_inh):

        all_r_peak_delays_dyn, all_t_thrs = [], []
        for rec_nr in range(self.n_recs_required):
            t_thr_crossings = self._get_t_thrs(N_osc_used=N_osc_used,
                                               all_events=all_events_over_recs[rec_nr],
                                               all_events_readout=all_events_readout_over_recs[rec_nr],
                                               neuron_ids_global_pops_exc=[neuron_ids_global_pop_exc],
                                               neuron_ids_global_pops_inh=[neuron_ids_global_pop_inh])

            t_max_considered = all_dt_runs[rec_nr]
            t_thr_crossings['exc0'] = t_thr_crossings['exc0'][np.where(np.vstack(t_thr_crossings['exc0']) < t_max_considered * 1e6)[0]]
            all_t_thrs.append(t_thr_crossings)

            rpeaks_indices = np.asarray(np.ceil(np.hstack(t_thr_crossings['exc0']) * 1e-6 / self.dt_ecg), dtype=int)
            _, r_peak_delays_dyn = self.ECG_trace.get_dataset_breathing_coeff_vs_r_peak_delays(rpeaks_indices=rpeaks_indices,
                                                                                               breathing_coefficient_trace= b_coeffs_trace_per_rec[rec_nr],
                                                                                               dt=self.dt_ecg)
            all_r_peak_delays_dyn.extend(list(r_peak_delays_dyn))

        return all_r_peak_delays_dyn, all_t_thrs

    def get_max_delay(self, osc_inv_params,
                      virtual_neuron_ids, chip_id_pop_exc, core_id_global_pop_exc, core_id_global_pop_inh,
                      neuron_ids_global_pop_exc, neuron_ids_global_pop_inh, main_core_mask,
                      dyn_params_SingleOsc_exc, current_EventBuffer,
                      thr_delay, adjusting_factor, exp_constA, scaling_factor,
                      return_run_data=False):

        # prepare oscillators
        self.set_osc_to_given_osc_period(osc_period=self.isi_init)
        self._switch_other_oscillators_off(core_id_global_pop_exc=core_id_global_pop_exc,
                                           core_id_global_pop_inh=core_id_global_pop_inh)  # ,
                                           # dyn_params_SingleOsc_exc=dyn_params_SingleOsc_exc)
        # switch on synapses from spikegen
        set_biases(self.dynapse_model, core_id_global_pop_exc, dyn_params_SingleOsc_exc)
        set_biases(self.dynapse_model, self.readout_chip_id*CORES_PER_CHIP+self.readout_core_id,
                   self.dyn_params_SpikeGenReadout)

        # collect events per recording
        # collect events per recording (split up into shorter recordings to have
        # spike times more accurate, as they get shifted over time of the recording on the FPGA)
        all_dt_runs = np.zeros(self.n_recs_required)
        all_events_over_recs, all_events_readout_over_recs, b_coeffs_trace_per_rec = [], [], []
        for rec_nr in range(self.n_recs_required):

            t_start = rec_nr * self.max_rec_duration
            t_end   = np.min((t_start + self.max_rec_duration, self.ECG_trace.t_rec[-1]))
            all_dt_runs[rec_nr] = (t_end - t_start)

            current_b_coeffs_trace, current_spike_freq_trace_per_osc, \
            current_t_spike_freq_trace, spike_times_per_osc = self.MyRSASG.get_spike_train_set_up_info(t_start=t_start, t_end=t_end,
                                                                                                ECG_trace=self.ECG_trace,
                                                                                                osc_inv_params_per_osc={0: osc_inv_params},
                                                                                                thr_delays_per_osc={0: thr_delay},
                                                                                                adjusting_factors_per_osc={0: adjusting_factor},
                                                                                                exp_constAs={0: exp_constA},
                                                                                                scaling_factors={0: scaling_factor},
                                                                                                seed_nr_poisson=self.seed_nr_poisson)

            self.MyRSASG.preload_dynapse_spikegen(spike_times_per_osc=spike_times_per_osc,
                                           spikegen=self.spikegen,
                                           MyFPGAEventGen=self.MyFPGAEventGen,
                                           virtual_neuron_ids=virtual_neuron_ids,
                                           spikegen_core_mask_per_osc={0: main_core_mask},
                                           spikegen_chip_ids_per_osc={0: chip_id_pop_exc},
                                           add_readout_events=True,
                                           n_spikes_to_readout=self.n_spikes_to_readout,
                                           readout_chip_id=self.readout_chip_id)

            all_events, all_events_readout = get_events_incl_spikegen_onset_delay_readout(BuffEF=current_EventBuffer,
                                                                                          BuffEF_spikegen_readout=self.BuffEF_spikegen_readout, dt_run=all_dt_runs[rec_nr],
                                                                                          my_spikegen=self.spikegen, CtxDynapse=self.CtxDynapse)
            all_events_over_recs.append(all_events)
            all_events_readout_over_recs.append(all_events_readout)
            b_coeffs_trace_per_rec.append(current_b_coeffs_trace)

        all_r_peak_delays_dyn, all_t_thrs = self._get_t_thr_delays_over_all_recordings(1, all_events_over_recs, all_events_readout_over_recs,
                                                  all_dt_runs, b_coeffs_trace_per_rec, neuron_ids_global_pop_exc, neuron_ids_global_pop_inh)

        median_nlargest_delays = np.median(heapq.nlargest(7, all_r_peak_delays_dyn))

        if return_run_data:
            return round_to_base(median_nlargest_delays, base=0.05), all_r_peak_delays_dyn, \
                                    b_coeffs_trace_per_rec, all_t_thrs
        else:
            return round_to_base(median_nlargest_delays, base=0.05)


    def update_adjusting_factor(self, max_delay, min_longest_delay, max_longest_delay,
                                current_adjusting_factor, step_size_adjusting_factor):

        DONE = False
        if max_delay>=min_longest_delay and max_delay<=max_longest_delay:
            DONE = True
        elif max_delay<min_longest_delay:
            current_adjusting_factor += step_size_adjusting_factor
            print('Max delay: {} - INCREASE to {}'.format(max_delay, current_adjusting_factor))
        elif max_delay>max_longest_delay:
            current_adjusting_factor -= step_size_adjusting_factor
            print('Max delay: {} - DECREASE to {}'.format(max_delay, current_adjusting_factor))

        return current_adjusting_factor, DONE


    def fit_adjusting_factor(self,
                             osc_inv_params, thr_delay, adjusting_factor,
                             exp_constA, scaling_factor,
                             virtual_neuron_ids, chip_id_pop_exc,
                             core_id_global_pop_exc, core_id_global_pop_inh,
                             neuron_ids_global_pop_exc, neuron_ids_global_pop_inh,
                             main_core_mask, dyn_params_SingleOsc_exc, current_EventBuffer,
                             min_longest_delay, max_longest_delay, step_size_adjusting_factor,
                             snapshot_every=1):

        counter = 0
        while True:
            counter += 1
            print('Counter', counter)
            max_delay, all_r_peak_delays_dyn, b_coeffs_trace_per_rec, all_t_thrs  = self.get_max_delay(osc_inv_params            = osc_inv_params,
                                                                                                       virtual_neuron_ids        = virtual_neuron_ids,
                                                                                                       chip_id_pop_exc           = chip_id_pop_exc,
                                                                                                       core_id_global_pop_exc    = core_id_global_pop_exc,
                                                                                                       core_id_global_pop_inh    = core_id_global_pop_inh,
                                                                                                       neuron_ids_global_pop_exc = neuron_ids_global_pop_exc,
                                                                                                       neuron_ids_global_pop_inh = neuron_ids_global_pop_inh,
                                                                                                       main_core_mask            = main_core_mask,
                                                                                                       dyn_params_SingleOsc_exc  = dyn_params_SingleOsc_exc,
                                                                                                       current_EventBuffer       = current_EventBuffer,
                                                                                                       thr_delay                 = thr_delay,
                                                                                                       adjusting_factor          = adjusting_factor,
                                                                                                       exp_constA                = exp_constA,
                                                                                                       scaling_factor            = scaling_factor,
                                                                                                       return_run_data           = True)

            adjusting_factor, DONE = self.update_adjusting_factor(max_delay=max_delay,
                                                                  min_longest_delay=min_longest_delay,
                                                                  max_longest_delay=max_longest_delay,
                                                                  current_adjusting_factor=adjusting_factor,
                                                                  step_size_adjusting_factor=step_size_adjusting_factor)
            if DONE:
                print('DONE! Final adjusting factor: {} . \n With max_delay of {}'.format(adjusting_factor, max_delay))
                break

            if counter%snapshot_every==0:
                self.take_snapshot_fit_adjusting_factor(b_coeffs_trace_per_rec, all_t_thrs)


    def update_scaling_factor(self, scaling_factors, step_size_scaling_factor,
                              t_thrs, osc_nr_leading_osc):

        # check if this parameter setting is ok (anti-phase oscillation)
        if self.N_osc == 2:
            pop_nr_pairs = [[0, 1], [1, 0]]
        elif self.N_osc == 3:
            pop_nr_pairs = [[0, 1], [1,2], [2, 0]]

        all_pop_name_pairs_ok = True
        for pop_nrs in pop_nr_pairs:
            # adjust provided activation times s.t. A-B comes only in couples (remove early B, and late A)
            pop_name_1 = 'exc{}'.format(pop_nrs[0])
            pop_name_2 = 'exc{}'.format(pop_nrs[1])

            adjusted_activation_times_pop2 = t_thrs[pop_name_2]
            if t_thrs[pop_name_2][0] < t_thrs[pop_name_1][0]:
                adjusted_activation_times_pop2 = t_thrs[pop_name_2][1:]
            adjusted_activation_times_pop1 = t_thrs[pop_name_1]
            if t_thrs[pop_name_1][-1] > t_thrs[pop_name_2][-1]:
                adjusted_activation_times_pop1 = t_thrs[pop_name_1][:-1]

            # get number of activations per population and check if some oscillations skipped --> inhibition to strong
            n_peaks_1 = len(adjusted_activation_times_pop1)
            n_peaks_2 = len(adjusted_activation_times_pop2)
            if n_peaks_1 != n_peaks_2:
                all_pop_name_pairs_ok = False
                if n_peaks_1 < n_peaks_2:
                    if pop_nrs[1] != osc_nr_leading_osc:
                        scaling_factors[pop_nrs[1]] += step_size_scaling_factor
                        print('{} has less peaks than {}, increase scaling_factors {} to: {}'.format(pop_name_1, pop_name_2, pop_name_2, scaling_factors[pop_nrs[1]]))
                elif n_peaks_2 < n_peaks_1:
                    if pop_nrs[0] != osc_nr_leading_osc:
                        scaling_factors[pop_nrs[0]] += step_size_scaling_factor
                        print('{} has less peaks than {}, increase scaling_factors {} to: {}'.format(pop_name_2, pop_name_1, pop_name_1, scaling_factors[pop_nrs[0]]))
                break

            else:
                # get delays between populations
                dt_pop1_to_pop2 = adjusted_activation_times_pop2 - adjusted_activation_times_pop1
                dt_pop2_to_pop1 = adjusted_activation_times_pop1[1:] - adjusted_activation_times_pop2[:-1]
                if (dt_pop1_to_pop2<0).any() or (dt_pop2_to_pop1<0).any():
                    all_pop_name_pairs_ok = False
                    if (dt_pop1_to_pop2<0).any():  # check if A too slow for B bzw B too fast for A --> decrease INH to A or increase INH to B
                        if pop_nrs[1] != osc_nr_leading_osc:
                            scaling_factors[pop_nrs[1]] += step_size_scaling_factor
                            print('{} spiked too late, increase scaling_factors {} to: {}'.format(pop_name_1, pop_name_2, scaling_factors[pop_nrs[1]]))
                    elif (dt_pop2_to_pop1<0).any():  # check if A too fast for B bzw B too slow for A --> increase INH to A or decrease INH to B
                        if pop_nrs[0] != osc_nr_leading_osc:
                            scaling_factors[pop_nrs[0]] += step_size_scaling_factor
                            print('{} spiked too late, increase scaling_factors {} to: {}'.format(pop_name_2, pop_name_1, scaling_factors[pop_nrs[0]]))
                    break

        return scaling_factors, all_pop_name_pairs_ok


    def fit_scaling_factors(self,
                            osc_nr_leading_osc,
                            osc_inv_params_per_osc,
                            thr_delays_per_osc,
                            adjusting_factors_per_osc,
                            exp_constAs,
                            scaling_factors,
                            step_size_scaling_factor,

                            virtual_neuron_ids,
                            chip_ids_per_osc,
                            core_mask_per_osc,

                            neuron_ids_global_pops_exc,
                            neuron_ids_global_pops_inh,

                            current_EventBuffer,
                            snapshot_every=1):

        # set up data collectors
        spike_freq_trace_per_osc_per_rec = {}
        for osc_nr in range(self.N_osc):
            spike_freq_trace_per_osc_per_rec[osc_nr] = []
        all_MyEMs, all_t_thrs = [], []
        all_events_over_recs, all_events_readout_over_recs = [], []

        rec_nr, counter = 0, 0
        print('start fine-tuning ... ')
        while rec_nr < self.n_recs_required:
            counter += 1

            # define recording start and end
            t_start = rec_nr * self.max_rec_duration
            t_end   = np.min((t_start +self.max_rec_duration, self.ECG_trace.t_rec[-1]))
            dt_run  = (t_end - t_start)

            all_events, all_events_readout, current_b_coeffs_trace, current_spike_freq_trace_per_osc, \
                current_t_spike_freq_trace, spike_times_per_osc = run_RSACPG_over_one_rec(t_start=t_start, t_end=t_end,
                                                                                            ECG_trace=self.ECG_trace,
                                                                                            osc_inv_params_per_osc=osc_inv_params_per_osc, thr_delays_per_osc=thr_delays_per_osc,
                                                                                            adjusting_factors_per_osc=adjusting_factors_per_osc, exp_constAs=exp_constAs,
                                                                                            scaling_factors=scaling_factors,
                                                                                            current_EventBuffer=current_EventBuffer, BuffEF_spikegen_readout=self.BuffEF_spikegen_readout,
                                                                                            spikegen=self.spikegen,
                                                                                            n_spikes_to_readout=self.n_spikes_to_readout,
                                                                                            virtual_neuron_ids=virtual_neuron_ids,
                                                                                            spikegen_core_mask_per_osc=core_mask_per_osc,
                                                                                            spikegen_chip_ids_per_osc=chip_ids_per_osc,
                                                                                            readout_chip_id=self.readout_chip_id,
                                                                                            MyRSASG=self.MyRSASG,
                                                                                            MyFPGAEventGen=self.MyFPGAEventGen,
                                                                                            CtxDynapse=self.CtxDynapse,
                                                                                            seed_nr_poisson=self.seed_nr_poisson,
                                                                                            return_all_data=True,
                                                                                            add_readout_events=True)

            MyEMs = OPlt.events_to_EMs_of_coupled_oscs(all_events=all_events,
                                                       N_osc=self.N_osc,
                                                       neuron_ids_global_pops_exc=neuron_ids_global_pops_exc,
                                                       neuron_ids_global_pops_inh=neuron_ids_global_pops_inh)
            correct_spiketimes_for_spike_generator_onset_delay(all_events_readout=all_events_readout, MyEMs_recordings=MyEMs)
            MySVMs = OPlt.EMs_to_SVMs_Atraces_of_coupled_osc(MyEMs, N_osc=self.N_osc, dt=self.dt, tau=self.tau)
            t_thrs = OPlt.SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(MySVMs=MySVMs, N_osc=self.N_osc,
                                                        thr_exc=self.thr_exc, thr_inh=self.thr_inh, verbose=False)

            if counter%snapshot_every == 0:
                self.take_snapshot_fit_scaling_factors(MyEMs=MyEMs,
                        t_spike_freq_trace=current_t_spike_freq_trace,
                        b_coeffs_trace=current_b_coeffs_trace, scaling_factors=scaling_factors,
                        rec_nr=rec_nr, dt_run=dt_run)

            scaling_factors, all_pop_name_pairs_ok = self.update_scaling_factor(scaling_factors=scaling_factors,
                                                                                step_size_scaling_factor=step_size_scaling_factor,
                                                                                t_thrs=t_thrs,
                                                                                osc_nr_leading_osc=osc_nr_leading_osc)

            if all_pop_name_pairs_ok:
                if counter == 1 or rec_nr == 0:  # go to next recording if this one was working without any changes in the params
                    print('\t Done with rec nr {}/{}, got to rec nr {}'.format(rec_nr, self.n_recs_required-1, rec_nr+1))
                    print('\t Current scaling_factors: ', scaling_factors)
                    rec_nr += 1
                    counter = 0
                    all_events_over_recs.append(all_events)
                    all_events_readout_over_recs.append(all_events_readout)
                    all_MyEMs.append(MyEMs)
                    all_t_thrs.append(t_thrs)
                    for osc_nr in range(self.N_osc):
                        spike_freq_trace_per_osc_per_rec[osc_nr].append(current_spike_freq_trace_per_osc[osc_nr])
                else:
                    print('\t Done with rec nr {}/{}, go BACK to rec nr 0'.format(rec_nr, self.n_recs_required-1))
                    print('\t Current scaling_factors: ', scaling_factors)
                    rec_nr, counter = 0, 0
                    all_events_over_recs, all_events_readout_over_recs = [], []
                    all_MyEMs, all_t_thrs = [], []
                    spike_freq_trace_per_osc_per_rec = {}
                    for osc_nr in range(self.N_osc):
                        spike_freq_trace_per_osc_per_rec[osc_nr] = []

        print('done, for scaling_factors:', scaling_factors)

    def take_snapshot_fit_adjusting_factor(self, b_coeffs_trace_per_rec,
                                           all_t_thrs):

        pop_name_exc = 'exc0'

        fig, ax = plt.subplots()
        for rec_nr in range(self.n_recs_required):
            rpeaks_indices = np.asarray(np.ceil(np.hstack(
                all_t_thrs[rec_nr][pop_name_exc]) * 1e-6 / self.dt_ecg),
                                        dtype=int)
            breathing_coeffs_dyn, r_peak_delays_dyn = self.ECG_trace.get_dataset_breathing_coeff_vs_r_peak_delays(
                rpeaks_indices=rpeaks_indices,
                breathing_coefficient_trace=b_coeffs_trace_per_rec[rec_nr],
                dt=self.dt_ecg)
            print('Rec nr {}: max r peak delay = {}'.format(rec_nr, np.max(
                r_peak_delays_dyn)))
            plt.scatter(breathing_coeffs_dyn, r_peak_delays_dyn, color='b',
                        label='DYNAP-SE rec nr:' + str(rec_nr), s=10)
            plt.scatter(breathing_coeffs_dyn,
                        self.MyRSASG.G(breathing_coeffs_dyn), marker='*', s=50,
                        color='r',
                        label='model rec nr:' + str(rec_nr))

        breathing_coeffs, r_peak_delays = self.ECG_trace.get_dataset_breathing_coeff_vs_r_peak_delays(
            rpeaks_indices=self.ECG_trace.rpeaks_indices,
            breathing_coefficient_trace=self.ECG_trace.breathing_coefficient_trace,
            dt=self.dt_ecg)
        plt.scatter(breathing_coeffs, r_peak_delays, color='orange', s=10,
                    label='ECG data')
        plt.xlabel('breathing coefficient')
        plt.ylabel('r peak delay')
        plt.legend()
        plt.show()


    def take_snapshot_fit_scaling_factors(self, MyEMs,
                                          t_spike_freq_trace,
                                          b_coeffs_trace,
                                          scaling_factors, rec_nr, dt_run):

        print("SNAPSHOT \n Current scaling_factors {} for rec nr {}: ".format(
            scaling_factors, rec_nr))
        fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(40, 5), sharex=True)
        RP = OPlt.get_rasterplot_of_coupled_oscs(MyEMs=MyEMs, N_osc=self.N_osc,
                                                 N_exc=self.N_exc,
                                                 N_inh=self.N_inh,
                                                 dt_run=dt_run,
                                                 squeeze_neuron_ids=True,
                                                 subfig=ax1)
        ax2.plot(t_spike_freq_trace * 1e6, b_coeffs_trace)
        plt.tight_layout()
        plt.show()
