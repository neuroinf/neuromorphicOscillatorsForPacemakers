# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

from brian2 import *
import numpy as np

class BiasValueConverter(object):
    ''' Class to help convert bias values (cores/fine value) to continuous values (estimation) '''

    def __init__(self):
        self.coarse_scales = [15 * pA, 105 * pA, 820 * pA, 6.5 * nA, 50 * nA, 0.4 * uA, 3.2 * uA, 24 * uA]

    def get_current(self, coarse_value, fine_value):
        """ Convert bias into current value: coarse_value: coarse value in range(8), fine_value: fine value in range(255) """
        maxCurrent = self.coarse_scales[coarse_value]
        return (fine_value / 256.) * maxCurrent

    def get_bias_values(self, current, verbose=True):
        """ Get coarse and fine value to describe given current. Pick the smallest coarse value possible so that the
        scale is as fine-grained as possible """
        FINE_VALUES = np.arange(256)
        I_MIN = self.coarse_scales[0] * np.min(FINE_VALUES)
        I_MAX = self.coarse_scales[-1]

        if current <= I_MIN:
            if verbose:
                print("Current value: '{}' is not valid. Bias value (0, 0) will be returned.".format(current))
            active_fine_value, active_coarse_value = 0, 0
        elif current >= I_MAX:
            if verbose:
                print("Current value: '{}' is not valid. Bias value (255, 7) will be returned.".format(current))
            active_fine_value, active_coarse_value = 255, 7
        else:
            valid_coarse_scales = [coarse_scale for coarse_scale in self.coarse_scales if coarse_scale >= current]
            active_coarse_scale = min(valid_coarse_scales)
            active_coarse_value = self.coarse_scales.index(active_coarse_scale)

            step_size = active_coarse_scale / max(FINE_VALUES)
            active_fine_value = np.round(current / step_size)

        return int(active_fine_value), int(active_coarse_value)
