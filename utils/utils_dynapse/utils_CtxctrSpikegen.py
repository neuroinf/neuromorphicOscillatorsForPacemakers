# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import time

from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import time_filter_events,events_to_array
from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter

OPlt = OscillatorPlotter()

def get_events_incl_spikegen_onset_delay_readout(BuffEF, BuffEF_spikegen_readout, dt_run, my_spikegen, CtxDynapse):
    """ collect events and tell tell offset of when spikeGenerator has sent the first spike
     (=infered with reference spike to control neuron which's events are collected with the Buffered EventsFilter
     BuffEF_spikegen_readout).
        BuffEF (ctxctr-bufferedEventFilter object): EventFilter for events to collect
        BuffEF_spikegen_readout (ctxctr-bufferedEventFilter object): EventFitler looking at reference neuron to infer spikeGen delay
        dt_run (float): duration of recording events
        my_spikegen (ctxctr- spike generator object):
     """

    # clean buffer and reset time
    ignored_events_readout = BuffEF_spikegen_readout.get_events()
    ignored_events = BuffEF.get_events()
    del ignored_events, ignored_events_readout
    CtxDynapse.dynapse.reset_timestamp()
    # start spikegen protocol
    my_spikegen.start()
    time.sleep(dt_run)
    # collect dynapse events & filter for dt to ensure that only events from within dt_run
    all_events = BuffEF.get_events()
    all_events_readout = BuffEF_spikegen_readout.get_events()
    all_events = time_filter_events(all_events=all_events, t_min=0, t_max=dt_run*1e6)
    all_events_readout = time_filter_events(all_events=all_events_readout, t_min=0, t_max=dt_run*1e6)

    return all_events, all_events_readout


def correct_spiketimes_for_spike_generator_onset_delay(all_events_readout, MyEMs_recordings):
    # remove offset spikegen (due to delay in actual start of spike generator)
    neuron_ids_readout, spiketimes_readout = events_to_array(all_events_readout)
    if len(spiketimes_readout) > 0:
        offset_spikegen = spiketimes_readout[0]
    else:
        offset_spikegen = 5000
        print('Readout did no spike, used default offset {}'.format(offset_spikegen))
    for pop_name, EM in MyEMs_recordings.items():
        EM.spike_times -= offset_spikegen
    return


def get_one_fpga_event(CtxDynapse, core_mask, target_chip, virtual_neuron_id, isi_fpga):
    fpga_event = CtxDynapse.FpgaSpikeEvent()
    fpga_event.core_mask = core_mask  # Receiving cores
    fpga_event.target_chip = target_chip  # Receiving chip
    fpga_event.neuron_id = virtual_neuron_id  # This needs to be the id of the virtual, i.e. sending, neuron
    fpga_event.isi = isi_fpga
    return fpga_event

def merge_separated_spike_trains_for_spikegen(spike_trains, core_mask_per_spike_train, target_chip_id_per_spike_train):

    n_spike_trains = len(spike_trains)
    counters = {}
    for spike_train_nr in range(n_spike_trains):
        counters[spike_train_nr] = 0

    all_spike_times = []
    all_core_masks = []
    all_target_chip_ids = []
    while True:

        next_spike_times = {}
        for spike_train_nr in range(n_spike_trains):
            spike_nr = counters[spike_train_nr]
            if spike_nr is not None:
                next_spike_times[spike_train_nr] = spike_trains[spike_train_nr][spike_nr]
            else:
                next_spike_times[spike_train_nr] = np.inf

        if np.isinf(list(next_spike_times.values())).all():
            break
        else:
            spike_train_nr_of_next_spike = min(next_spike_times, key=next_spike_times.get)
            spike_nr = counters[spike_train_nr_of_next_spike]
            all_spike_times.append(spike_trains[spike_train_nr_of_next_spike][spike_nr])
            all_core_masks.append(core_mask_per_spike_train[spike_train_nr_of_next_spike])
            all_target_chip_ids.append(target_chip_id_per_spike_train[spike_train_nr_of_next_spike])

            if counters[spike_train_nr_of_next_spike] >= len(spike_trains[spike_train_nr_of_next_spike])-1:
                counters[spike_train_nr_of_next_spike] = None
            else:
                counters[spike_train_nr_of_next_spike] += 1

    return all_spike_times, all_core_masks, all_target_chip_ids


