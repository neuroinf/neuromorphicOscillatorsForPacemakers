# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

class DynapseOffSwitcher(object):
    """ Class with methods to set DYNAP-SE biases to switch off specific functionalities"""

    def __init__(self, CtxDynapse):
        self.CtxDynapse = CtxDynapse


    def switch_off_all_neurons(self, dynapse_model=None, cores=range(16), verbose=True):
        """
        dynapse_model: cortexcontrol dynapse model
        cores (list): list of cores where neurons should be switched off
        """

        if dynapse_model is None:
            model = self.CtxDynapse.model
        else:
            model = dynapse_model

        all_bias_groups = model.get_bias_groups()
        for nr_bias_group in cores:
            bias_group = all_bias_groups[nr_bias_group]
            bias_group.set_bias('IF_DC_P', 0, 0)
            bias_group.set_bias('IF_TAU1_N', 255, 7)
            bias_group.set_bias('IF_TAU2_N', 255, 7)

        if verbose:
            print('done, all neurons should be switched OFF')
        return


    def switch_off_all_synapses(self, dynapse_model=None, cores=range(16), verbose=True):
        """
        dynapse_model: cortexcontrol dynapse model
        cores (list): list of cores where synapses should be switched off
        """
        if dynapse_model is None:
            model = self.CtxDynapse.model
        else:
            model = dynapse_model

        all_bias_groups = model.get_bias_groups()
        for nr_bias_group in cores:
            bias_group = all_bias_groups[nr_bias_group]
            for tau_parameter in ['NPDPIE_TAU_F_P', 'NPDPIE_TAU_S_P', 'NPDPII_TAU_F_P', 'NPDPII_TAU_S_P']:
                bias_group.set_bias(tau_parameter, 255, 7)
            for weight_paramter in ['PS_WEIGHT_EXC_F_N', 'PS_WEIGHT_EXC_S_N', 'PS_WEIGHT_INH_F_N', 'PS_WEIGHT_INH_S_N']:
                bias_group.set_bias(weight_paramter, 0, 0)

        if verbose:
            print('done, all synapses should be switched OFF')
        return


    def switch_off_specific_synpases(self, dynapse_model, bias_group_nrs, syn_types, verbose=True):
        """
        dynapse_model: cortexcontrol dynapse model
        bias_group_nrs (list): list of bias group numbers where synpases should be switched off
        syn_types (list): list of synapse types that should be switched of on ALL the bias groups in bias_group_nrs
        """

        for bias_group_nr in bias_group_nrs:
            if 'fast_exc' in syn_types:
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_TAU_F_P', 255, 7)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_THR_F_P', 0, 0)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_EXC_F_N', 0, 0)
            if 'slow_exc' in syn_types:
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_TAU_S_P', 255, 7)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPIE_THR_S_P', 0, 0)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_EXC_S_N', 0, 0)
            if 'fast_inh' in syn_types:
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_TAU_F_P', 255, 7)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_THR_F_P', 0, 0)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_INH_F_N', 0, 0)
            if 'slow_inh' in syn_types:
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_TAU_S_P', 255, 7)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('NPDPII_THR_S_P', 0, 0)
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias('PS_WEIGHT_INH_S_N', 0, 0)

        if verbose:
            print('done, {} switched off'.format(syn_types))
        return


    def switch_off_adaptation(self, dynapse_model=None, cores=range(16), verbose=True):
        """
        dynapse_model: cortexcontrol dynapse model
        cores (list): list of cores where adaptation should be switched off
        """

        if dynapse_model is None:
            model = self.CtxDynapse.model
        else:
            model = dynapse_model

        #  IF_BUF_P
        all_bias_groups = model.get_bias_groups()
        for nr_bias_group in cores:
            bias_group = all_bias_groups[nr_bias_group]
            bias_group.set_bias('IF_AHTAU_N', 255, 7)
            bias_group.set_bias('IF_AHTHR_N', 0, 0)
            bias_group.set_bias('IF_AHW_P', 0, 0)
            bias_group.set_bias('IF_CASC_N', 0, 0)

        if verbose:
            print('done, spike-frequency adaptation should be switched OFF')
        return


    def switch_off_NMDA(self, dynapse_model=None, cores=range(16), verbose=True):
        """
        dynapse_model: cortexcontrol dynapse model
        cores (list): list of cores where NMDA parameter should be switched off
        """
        if dynapse_model is None:
            model = self.CtxDynapse.model
        else:
            model = dynapse_model

        all_bias_groups = model.get_bias_groups()
        for nr_bias_group in cores:
            bias_group = all_bias_groups[nr_bias_group]
            bias_group.set_bias('IF_NMDA_N', 0, 0)

        if verbose:
            print('done, IF_NMDA_N set to zero')
        return
