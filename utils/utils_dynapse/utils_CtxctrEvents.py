# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import time

from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.utils_tuningPreparation import get_global_neuron_id

def time_filter_events(all_events, t_min, t_max):
    """ events: list of ctxctr-events """
    # remove all event with a timestamp outside of t_min, t_max
    all_events_filtered = []
    for event in list(all_events):
        if event.timestamp < t_max and event.timestamp > t_min:
            all_events_filtered.append(event)
    return all_events_filtered


def get_events_within_dt(dt_init, dt_run, current_EventBuffer, CtxDynapse, verbose=True):
    '''
    dt_init (float): time to wait before starting to collect events (in seconds)
    dt_run (float): duration of time to collect events (in seconds)
    current_EventBuffer: CtxCtr bufferedEventsfilter to use to collect events from
    '''
    # wait a bit to adjust to new biases
    time.sleep(dt_init)

    if verbose:
        print('start recording ... ')
    # clean buffer and reset time
    ignored_events = current_EventBuffer.get_events()  # req. to empty buffer
    del ignored_events

    CtxDynapse.dynapse.reset_timestamp()

    # wait and collect dynapse events, and filter for dt to ensure that only
    # events from within the dt_run are returned
    time.sleep(dt_run)
    all_events = current_EventBuffer.get_events()
    all_events = time_filter_events(all_events=all_events, t_min=0, t_max=dt_run*1e6)

    if verbose:
        print('... got all the events \o/')

    return all_events


def events_to_array(events):
    ''' convert events from one object per event to array of neuron ids and spike times '''
    n_events = len(events)
    spiketimes = np.zeros(n_events)
    neuron_ids = np.zeros(n_events)

    for spike_nr, spike in enumerate(events):
        global_neuron_id_spike = get_global_neuron_id(spike.neuron.get_neuron_id(), spike.neuron.get_core_id(),
                                                      spike.neuron.get_chip_id())
        neuron_ids[spike_nr] = global_neuron_id_spike
        spiketimes[spike_nr] = spike.timestamp

    return neuron_ids, spiketimes


def squeeze_neuron_ids(neuron_ids_A, neuron_ids_B, range_neuron_ids_A=None, range_neuron_ids_B=None):
    ''' adjust neuron ids to be closeby to simply plotting them (remove empty rows'''
    # squeeze neuron ids of group A and B so that neuron_ids_B are directly adjacent to neuron_ids_B
    # start from 0 or range_neuron_id_A[0] (= min neuron id in A)
    # shift pop A to start at 1
    if range_neuron_ids_A is None:
        if len(neuron_ids_A) == 0:
            min_neuron_id_A = 0
            max_neuron_id_A = 1
        else:
            min_neuron_id_A = np.min(np.unique(neuron_ids_A))
            neuron_ids_A = np.asarray(neuron_ids_A) - min_neuron_id_A + 1  # +1 to avoid neuron id 0
            max_neuron_id_A = np.max(np.unique(neuron_ids_A))
    else:
        min_neuron_id_A = range_neuron_ids_A[0]
        max_neuron_id_A = range_neuron_ids_A[1]
        neuron_ids_A = np.asarray(neuron_ids_A) - min_neuron_id_A + 1  # +1 to avoid neuron id 0

    # shift pop B to start right after pop A
    if range_neuron_ids_B is None:
        if len(neuron_ids_B) == 0:
            min_neuron_id_B = max_neuron_id_A + 1
        else:
            min_neuron_id_B = np.min(np.unique(neuron_ids_B))
            neuron_ids_B = np.asarray(neuron_ids_B) - min_neuron_id_B + max_neuron_id_A + 1
    else:
        min_neuron_id_B = range_neuron_ids_B [0]
        neuron_ids_B = np.asarray(neuron_ids_B) - min_neuron_id_B + max_neuron_id_A + 1

    return neuron_ids_A, neuron_ids_B


def split_events_into_subgroups(all_events, ids_per_subgroup):
    ''' split events into subgroups e.g. inh vs exc population '''
    # ids_per_subgroup: dict with name_of_subgroup to neuron_idS_of_this_subgroup
    #   (e.g. {'excA': [192, 193, 194, 195], 'inhA': [200, 201, 2020, 203]}

    # init dictionary entries with empty lists
    events_per_subgroups = {}
    for subgroup_name in ids_per_subgroup.keys():
        events_per_subgroups[subgroup_name] = []

    # filter events
    for spike in all_events:
        global_neuron_id_spike = get_global_neuron_id(spike.neuron.get_neuron_id(),
                                                      spike.neuron.get_core_id(),
                                                      spike.neuron.get_chip_id())
        for subgroup_name, subgroup_ids in ids_per_subgroup.items():
            if global_neuron_id_spike in subgroup_ids:
                events_per_subgroups[subgroup_name].append(spike)
                break

    return events_per_subgroups

