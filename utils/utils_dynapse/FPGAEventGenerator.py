# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

class FPGAEventGenerator(object):
    """ Class used to generate FPGA events to send from the DYNAP-SE board"""
    
    def __init__(self,
                 fill_in_target_chip,
                 fill_in_core_mask,
                 fill_in_neuron_id,
                 max_fpga_events,
                 max_isi=0.6,  # max_isi = 0.60  # seconds, tested with isi_base=900
                 CtxDynapse=None):
        """
        Class to generate class::events which can the be used to pass it the FPGA-spikegen

        Creates events based on provided spike times and automaticall created
        "fill in events" if the ISI between two spikes is too long.

        :param fill_in_target_chip: (int), chip nr of chip where the fill in event should be sent to
        :param fill_in_core_mask:  (int), core mask for events where the fill in event should be sent to
        :param fill_in_neuron_id: (int), core mask for events where the fill in event should be sent to
        :param max_fpga_events:  (int), neuron id of sending/virtual neuron
        :param max_isi: (float), maximal isi which can be used without having to introduce a fill-in event
        :param CtxDynapse: CortexControl Module
        """

        self.fill_in_target_chip = fill_in_target_chip
        self.fill_in_core_mask = fill_in_core_mask
        self.fill_in_neuron_id = fill_in_neuron_id

        self.max_fpga_events = max_fpga_events
        self.max_isi = max_isi

        self.CtxDynapse = CtxDynapse


    def get_DYNAPSE_isi(self, isi_seconds, isi_base=900, device_multiplier=11*1e-9):
        """ Get dynapse isi used in FPGAEvent class for spike to send to FPGA-spikegen
        :param isi_seconds: (float), isi in seconds
        :param isi_base: (int), isi base, e.g. 900
        :param device_multiplier: (float), ? set to: 11*1e-9
        :return:
        """
        isi = (isi_seconds*1e9)/(isi_base*device_multiplier)
        return int(isi*1e-9)

    def get_fpga_events(self, isis, main_core_masks, main_target_chips, main_neuron_ids, verbose=True):
        """
        Generate list of FPGA events for DYNAP-SE
        :param isis: (list, array), inter-spike-intervals in seconds
        :param main_core_masks: list of mask of receiving cores for events
        :param main_target_chips: list of chip id of receiving chip
        :param main_neuron_ids: list of neuron id of sending/virtual neuron
        :return:
        """
        fpga_events = []
        for isi_nr, isi in enumerate(isis):
            while True:
                try:
                    fpga_event = self.CtxDynapse.FpgaSpikeEvent()
                    isi_fpga = self.get_DYNAPSE_isi(isi_seconds=isi)
                    fpga_event.isi = isi_fpga
                    fpga_event.core_mask   = main_core_masks[isi_nr]    # Receiving cores
                    fpga_event.target_chip = main_target_chips[isi_nr]  # Receiving chipdf
                    fpga_event.neuron_id   = main_neuron_ids[isi_nr]    # This needs to be the id of the virtual, i.e. sending, neuron
                    break
                except:
                    fpga_event = self.CtxDynapse.FpgaSpikeEvent()
                    isi_fpga = self.get_DYNAPSE_isi(isi_seconds=self.max_isi)
                    fpga_event.isi = isi_fpga
                    fpga_event.core_mask   = self.fill_in_core_mask    # Receiving cores
                    fpga_event.target_chip = self.fill_in_target_chip  # Receiving chipdf
                    fpga_event.neuron_id   = self.fill_in_neuron_id    # This needs to be the id of the virtual, i.e. sending, neuron
                    isi = isi - self.max_isi
            fpga_events.append(fpga_event)

        if verbose:
            print('Num fpga events:', len(fpga_events))

        return fpga_events

