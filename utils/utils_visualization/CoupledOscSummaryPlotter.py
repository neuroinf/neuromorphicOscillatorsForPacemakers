# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np

from neuromorphicOscillatorsForPacemakers.CPG_model.ActivationTimeExtractor import ActivationTimeExtractor
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import get_events_within_dt
from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter

ATE = ActivationTimeExtractor()
OPlt = OscillatorPlotter()


class CoupledOscSummaryPlotter(object):
    """ Class to plot events collected from coupled oscillators on DYNAP-SE """

    def __init__(self, CtxDynapse, N_osc, N_exc, N_inh,
                 neuron_ids_global_pops_exc, neuron_ids_global_pops_inh):

        self.CtxDynapse = CtxDynapse

        self.N_osc = N_osc
        self.N_exc = N_exc
        self.N_inh = N_inh

        self.neuron_ids_global_pops_exc = neuron_ids_global_pops_exc
        self.neuron_ids_global_pops_inh = neuron_ids_global_pops_inh


    def plot_coupled_oscillator_summary(self, dt_init, dt_run,
                                        BuffEF_all,
                                        thr_exc, thr_inh, dt, tau, fontsize=16,
                                        figsize_rasterplot=(20, 3), verbose=True):

        all_events = get_events_within_dt(dt_init=dt_init,
                                          dt_run=dt_run,
                                          current_EventBuffer=BuffEF_all,
                                          CtxDynapse=self.CtxDynapse,
                                          verbose=verbose)

        MyEMs = OPlt.events_to_EMs_of_coupled_oscs(all_events=all_events,
                                                   N_osc=self.N_osc,
                                                   neuron_ids_global_pops_exc=self.neuron_ids_global_pops_exc,
                                                   neuron_ids_global_pops_inh=self.neuron_ids_global_pops_inh)

        RP = OPlt.get_rasterplot_of_coupled_oscs(MyEMs=MyEMs,
                                                 N_osc=self.N_osc,
                                                 N_exc=self.N_exc,
                                                 N_inh=self.N_inh,
                                                 dt_run=dt_run,
                                                 figsize=figsize_rasterplot,
                                                 squeeze_neuron_ids=True,
                                                 fontsize=fontsize)

        MySVMs = OPlt.EMs_to_SVMs_Atraces_of_coupled_osc(MyEMs,
                                                         N_osc=self.N_osc,
                                                         dt=dt,
                                                         tau=tau)

        t_thr_crossings = OPlt.SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(MySVMs=MySVMs,
                                                                                     N_osc=self.N_osc,
                                                                                     thr_exc=thr_exc,
                                                                                     thr_inh=thr_inh,
                                                                                     verbose=verbose)

        if self.N_osc == 2:
            osc_pairs_to_compare = [[0,1]]
        elif self.N_osc == 3:
            osc_pairs_to_compare = [[0,1], [1,2], [2,0]]

        for pop_nrs in osc_pairs_to_compare:
            pop_name_A = 'exc{}'.format(pop_nrs[0])
            pop_name_B = 'exc{}'.format(pop_nrs[1])
            dt_popA_to_popB, dt_popB_to_popA = ATE.get_delays_between_two_populations(activation_times_popA=np.asarray(t_thr_crossings[pop_name_A], dtype=int),
                                                                                      activation_times_popB=np.asarray(t_thr_crossings[pop_name_B], dtype=int),
                                                                                      check_alternation=False)
            if verbose:
                print("Average delays {}-{}: {}+-{} ms, Average delays {}-{}: {}+-{} ms".format(pop_name_A, pop_name_B,
                                                                                                round(np.mean(dt_popA_to_popB)*1e-3, 2),
                                                                                                round(np.std(dt_popA_to_popB)*1e-3, 2),
                                                                                                pop_name_B, pop_name_A,
                                                                                                round(np.mean(dt_popB_to_popA)*1e-3, 2),
                                                                                                round(np.std(dt_popB_to_popA)*1e-3, 2)))

        return
