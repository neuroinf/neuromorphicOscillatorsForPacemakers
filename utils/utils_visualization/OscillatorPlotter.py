# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import matplotlib.pylab as plt

from neuromorphicOscillatorsForPacemakers.CPG_model.ActivationTimeExtractor import ActivationTimeExtractor
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import events_to_array, squeeze_neuron_ids, split_events_into_subgroups

from teili.tools.visualizer.DataViewers import PlotSettings
from teili.tools.visualizer.DataModels import EventsModel, StateVariablesModel
from teili.tools.visualizer.DataControllers import Rasterplot, Lineplot

ATE = ActivationTimeExtractor()


class OscillatorPlotter(object):
    """ Class to visualize events of oscillators running on DYNAP-SE """

    def summaryplot_single_oscillator(self, neuron_ids_exc, spiketimes_exc, neuron_ids_inh, spiketimes_inh,
                                      dt=10., tau=0.5*1e6, dt_run=30., thr_exc=None, thr_inh=None):

        # visualize individual bias settings
        MyEM_exc = EventsModel(neuron_ids=neuron_ids_exc, spike_times=spiketimes_exc)
        MyEM_inh = EventsModel(neuron_ids=neuron_ids_inh, spike_times=spiketimes_inh)
        MyEM_exc.neuron_ids, MyEM_inh.neuron_ids = squeeze_neuron_ids(neuron_ids_A=MyEM_exc.neuron_ids,
                                                                      neuron_ids_B=MyEM_inh.neuron_ids)

        A_exc, A_t_exc = ATE.get_average_activitiy_trace(MyEM_exc.spike_times, dt=dt, tau=tau, A_step=1.)
        A_inh, A_t_inh = ATE.get_average_activitiy_trace(MyEM_inh.spike_times, dt=dt, tau=tau, A_step=1.)

        if thr_exc is None:
            thr_exc = 0.7 * np.max(A_exc)
        if thr_inh is None:
            thr_inh = 0.7 * np.max(A_inh)

        t_thr_crossing_exc = ATE.get_t_threshold_crossing(A=A_exc, A_t=A_t_exc, thr=thr_exc)
        t_thr_crossing_inh = ATE.get_t_threshold_crossing(A=A_inh, A_t=A_t_inh, thr=thr_inh)
        t_thr_crossing_exc, t_thr_crossing_inh = ATE.remove_double_peaks(activation_times_exc=t_thr_crossing_exc,
                                                                         activation_times_inh=t_thr_crossing_inh)

        print('isi exc: {} +- {}ms'.format(round(np.mean(np.diff(t_thr_crossing_exc.flatten()))*1e-3, 2),
                                         round(np.std(np.diff(t_thr_crossing_exc.flatten()))*1e-3, 2)))

        # Raster plot
        fig, (ax_raster, ax_Atrace) = plt.subplots(ncols=1, nrows=2, figsize=(20,9))
        RP = Rasterplot(MyEventsModels=[MyEM_exc, MyEM_inh], MyPlotSettings=PlotSettings(marker_size=15), subgroup_labels=['exc', 'inh'],
                   time_range=(0, dt_run*1e6), ylabel='neuron id', mainfig=fig, subfig_rasterplot=ax_raster)
        ax_raster.legend(bbox_to_anchor=(1.1, 1.05))

        # Line plot
        SVM_Atrace = StateVariablesModel(state_variable_names=['A_exc', 'A_inh'], state_variables=[A_exc, A_inh], state_variables_times=[A_t_exc, A_t_inh])
        LP = Lineplot(DataModel_to_x_and_y_attr=[(SVM_Atrace, ('t_A_exc', 'A_exc')), (SVM_Atrace, ('t_A_inh', 'A_inh'))], MyPlotSettings=PlotSettings(), subgroup_labels=['A_exc', 'A_inh'],
                      x_range=(0, dt_run*1e6), title='A-trace', xlabel='time', ylabel='A', mainfig=fig, subfig=ax_Atrace, show_immediately=False)
        ax_Atrace.legend(bbox_to_anchor=(1.1, 1.05))
        plt.tight_layout()
        for t in t_thr_crossing_exc:
            ax_Atrace.axvline(x=t, c='k', ls='-.', linewidth=1)
        for t in t_thr_crossing_inh:
            ax_Atrace.axvline(x=t, c='g', ls='-.', linewidth=1)
        return

    def events_to_EMs_of_coupled_oscs(self, all_events, N_osc, neuron_ids_global_pops_exc,
                                     neuron_ids_global_pops_inh):

        ids_per_subgroup = {}
        for osc_nr in range(N_osc):
            ids_per_subgroup['exc'+str(int(osc_nr))] = neuron_ids_global_pops_exc[osc_nr]
            ids_per_subgroup['inh'+str(int(osc_nr))] = neuron_ids_global_pops_inh[osc_nr]
        events_per_subgroup = split_events_into_subgroups(all_events, ids_per_subgroup=ids_per_subgroup)

        MyEMs = {}
        for osc_nr in range(N_osc):
            neuron_ids_exc, spiketimes_exc = events_to_array(events_per_subgroup['exc'+str(int(osc_nr))])
            neuron_ids_inh, spiketimes_inh = events_to_array(events_per_subgroup['inh'+str(int(osc_nr))])
            MyEMs['exc'+str(int(osc_nr))] = EventsModel(neuron_ids=neuron_ids_exc, spike_times=spiketimes_exc)
            MyEMs['inh'+str(int(osc_nr))] = EventsModel(neuron_ids=neuron_ids_inh, spike_times=spiketimes_inh)

        return MyEMs

    def EMs_to_SVMs_Atraces_of_coupled_osc(self, MyEMs, N_osc, dt, tau):
        MySVMs = {}
        A_traces, A_t_traces = {}, {}
        for osc_nr in range(N_osc):
            name_exc = 'exc'+str(int(osc_nr))
            name_inh = 'inh'+str(int(osc_nr))
            A_traces[name_exc], A_t_traces[name_exc] = ATE.get_average_activitiy_trace(MyEMs[name_exc].spike_times, dt=dt, tau=tau, A_step=1.)
            A_traces[name_inh], A_t_traces[name_inh] = ATE.get_average_activitiy_trace(MyEMs[name_inh].spike_times, dt=dt, tau=tau, A_step=1.)
            MySVMs[str(osc_nr)] = StateVariablesModel(state_variable_names=['A_exc', 'A_inh'],
                                                      state_variables=[A_traces[name_exc], A_traces[name_inh]],
                                                      state_variables_times=[A_t_traces[name_exc], A_t_traces[name_inh]])
        return MySVMs

    def SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(self, MySVMs, N_osc, thr_exc, thr_inh, verbose=True, remove_double_peaks=True):
        t_thr_crossings = {}
        for osc_nr in range(N_osc):
            name_exc = 'exc'+str(int(osc_nr))
            name_inh = 'inh'+str(int(osc_nr))
            t_thr_crossings[name_exc] = ATE.get_t_threshold_crossing(A=MySVMs[str(osc_nr)].A_exc, A_t=MySVMs[str(osc_nr)].t_A_exc, thr=thr_exc).flatten()
            t_thr_crossings[name_inh] = ATE.get_t_threshold_crossing(A=MySVMs[str(osc_nr)].A_inh, A_t=MySVMs[str(osc_nr)].t_A_inh, thr=thr_inh).flatten()

            # ensure that formatting of t_thr_crossings is correct
            for pop_name in [name_exc, name_inh]:
                if len(t_thr_crossings[pop_name]) > 0:
                    t_thr_crossings[pop_name] = np.hstack(t_thr_crossings[pop_name]).flatten()

            if remove_double_peaks:
                t_thr_crossings[name_exc], t_thr_crossings[name_inh] = ATE.remove_double_peaks(activation_times_exc=t_thr_crossings[name_exc].flatten(),
                                                                                               activation_times_inh=t_thr_crossings[name_inh].flatten())

            # ensure that formatting of t_thr_crossings is correct
            for pop_name in [name_exc, name_inh]:
                if len(t_thr_crossings[pop_name]) > 0:
                    t_thr_crossings[pop_name] = np.hstack(t_thr_crossings[pop_name]).flatten()

            if verbose:
                print('ISI Exc (Osc {}): {} +- {}ms'.format(osc_nr,
                                                          round(np.mean(np.diff(t_thr_crossings[name_exc].flatten()))*1e-3, 2),
                                                          round(np.std(np.diff(t_thr_crossings[name_exc].flatten())))*1e-3, 2))
                print('ISI Inh (Osc {}): {} +- {}ms'.format(osc_nr,
                                                          round(np.mean(np.diff(t_thr_crossings[name_inh].flatten()))*1e-3, 2),
                                                          round(np.std(np.diff(t_thr_crossings[name_inh].flatten())))*1e-3, 2))

        return t_thr_crossings


    def squeeze_neuron_ids_of_oscillators(self, MyEMs, N_osc, N_exc, N_inh, start_neuron_id_global_per_subgroup=None):
        """
        :param MyEMs: dict of teili Eventsmodels
        :param N_osc: (int), number of oscillators in MyEMs
        :param N_exc: (list of int), list of number of excitatory neurons per oscillator
        :param N_inh: (list of int), list of number of inhibitory neurons per oscillator
        :param start_neuron_id_global_per_subgroup: (dict), similar to MyEMs a dict with the
                                                    same keys but mapping to the neuron_id_global
                                                    where each neuron population starts
        :return:
        """
        # squeeze neuron ids
        for osc_nr in range(N_osc):
            # if not provided, simply squeeze neurons together
            if start_neuron_id_global_per_subgroup is None:
                MyEMs['exc' + str(int(osc_nr))].neuron_ids, MyEMs['inh'+str(int(osc_nr))].neuron_ids = squeeze_neuron_ids(neuron_ids_A=MyEMs['exc'+str(int(osc_nr))].neuron_ids,
                                                                                                                        neuron_ids_B=MyEMs['inh'+str(int(osc_nr))].neuron_ids)
            # if more information provided, make sure distance between neurons within population are preserved
            else:
                MyEMs['exc'+str(int(osc_nr))].neuron_ids -= start_neuron_id_global_per_subgroup['exc'+str(int(osc_nr))]
                MyEMs['inh'+str(int(osc_nr))].neuron_ids = MyEMs['inh'+str(int(osc_nr))].neuron_ids \
                                            - start_neuron_id_global_per_subgroup['inh'+str(int(osc_nr))] + N_exc[osc_nr]

            if osc_nr > 0:
                for nr in range(osc_nr):
                    MyEMs['exc'+str(int(osc_nr))].neuron_ids += N_exc[nr]+N_inh[nr]
                    MyEMs['inh'+str(int(osc_nr))].neuron_ids += N_exc[nr]+N_inh[nr]

        return MyEMs


    def get_rasterplot_of_coupled_oscs(self, MyEMs, N_osc, N_exc, N_inh, dt_run,
                                       subfig=None, figsize=(20, 3), colors=None,
                                       start_neuron_id_global_per_subgroup=None,
                                       squeeze_neuron_ids=False, fontsize=16):

        if squeeze_neuron_ids:
            MyEMs = self.squeeze_neuron_ids_of_oscillators(MyEMs=MyEMs, N_osc=N_osc, N_exc=N_exc, N_inh=N_inh,
                                                           start_neuron_id_global_per_subgroup=start_neuron_id_global_per_subgroup)

        if subfig is None:
            fig, subfig = plt.subplots(figsize=figsize)

        EMs_to_show, subgroup_labels = [], []
        for osc_nr in range(N_osc):
            name_exc = 'exc'+str(int(osc_nr))
            name_inh = 'inh'+str(int(osc_nr))
            EMs_to_show.append(MyEMs[name_exc])
            EMs_to_show.append(MyEMs[name_inh])
            subgroup_labels.append(name_exc)
            subgroup_labels.append(name_inh)

        if colors is None:
            colors = ['r', 'b', 'g', 'c', 'k', 'm', 'y']

        RP = Rasterplot(MyEventsModels=EMs_to_show,
                   MyPlotSettings=PlotSettings(marker_size=15, colors=colors), subgroup_labels=subgroup_labels,
                   time_range=(0, dt_run*1e6), title='', xlabel='time', ylabel='neuron id', # mainfig=fig
                   subfig_rasterplot=subfig, show_immediately=False)
        subfig.legend(bbox_to_anchor=(1.1, 1.05), fontsize=fontsize)

        return RP

    def get_lineplot_Atraces_of_coupled_oscs(self, MySVMs, t_thr_crossings, dt_run,
                                             subfigs_indiv_plots=None, subfig_shared_plot=None,
                                             colors=None):

        if colors is None:
            colors = ['r', 'b', 'g', 'c', 'k', 'm', 'y']

        N_osc = len(MySVMs.keys())

        LPs = []
        t_thr_crossings_to_show = []
        for osc_nr in range(N_osc):
            t_thr_crossings_to_show.append(t_thr_crossings['exc'+str(int(osc_nr))])

        if subfigs_indiv_plots is not None:
            for osc_nr, ax in zip(range(N_osc), subfigs_indiv_plots):
                pop_name = str(int(osc_nr))
                SVM = MySVMs[pop_name]
                LP = Lineplot(DataModel_to_x_and_y_attr=[(SVM, ('t_A_exc', 'A_exc')), (SVM, ('t_A_inh', 'A_inh'))],
                              MyPlotSettings=PlotSettings(colors=colors), subgroup_labels=['A_exc', 'A_inh'],
                              x_range=(0, dt_run*1e6), title='A-trace ' + pop_name, xlabel='time',
                              ylabel='A', subfig=ax, show_immediately=False)
                ax.legend(bbox_to_anchor=(1.1, 1.05))
                LPs.append(LP)
            plt.tight_layout()

            for t_thr_crossing, ax in zip(t_thr_crossings_to_show, subfigs_indiv_plots):
                for t in t_thr_crossing:
                    ax.axvline(x=t, c='k', ls='-.', linewidth=1)

        if subfig_shared_plot is not None:
            datamodel_to_x_y_attr, subgroup_labels = [], []
            for osc_nr in range(N_osc):
                name = str(int(osc_nr))
                datamodel_to_x_y_attr.append((MySVMs[name], ('t_A_exc', 'A_exc')))
                subgroup_labels.append('A_exc_'+name)
            LP = Lineplot(DataModel_to_x_and_y_attr=datamodel_to_x_y_attr,
                          MyPlotSettings=PlotSettings(colors=colors), subgroup_labels=subgroup_labels,
                          x_range=(0, dt_run*1e6), title='A-trace (all)', xlabel='time', ylabel='A', subfig=subfig_shared_plot, show_immediately=False)
            LPs.append(LP)
            subfig_shared_plot.legend(bbox_to_anchor=(1.1, 1.05))
            plt.tight_layout()
            for t_thr_nr, t_thr_crossing in enumerate(t_thr_crossings_to_show):
                for t in t_thr_crossing:
                    subfig_shared_plot.axvline(x=t, c=colors[t_thr_nr], ls='--', linewidth=2, alpha=0.5)
        return LPs

    def plot_stim_trace(self, stim_trace, t_stim_trace=None, xlabel='time',
                            ylabel='stimulus', subfig=None, figsize=(20,3), fontsize=12):
        if subfig is None:
            fig, subfig = plt.subplots(figsize=figsize)

        if t_stim_trace is None:
            t_stim_trace = np.arange(len(stim_trace))

        subfig.grid()
        subfig.plot(t_stim_trace, stim_trace)
        subfig.set_xlabel(xlabel, fontsize=fontsize)
        subfig.set_ylabel(ylabel, fontsize=fontsize)
        return subfig
